/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.buildpath.core;

import org.eclipse.core.runtime.IPath;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public interface BuildpathElement {
	
	
	String SOURCE= "Source"; //$NON-NLS-1$
	String PROJECT= "Project"; //$NON-NLS-1$
	String LIBRARY= "Library"; //$NON-NLS-1$
	String VARIABLE= "Variable"; //$NON-NLS-1$
	
	
	BuildpathElementType getType();
	String getTypeName();
	
	IPath getPath();
	
	@Nullable ImList<IPath> getInclusionPatterns();
	@Nullable ImList<IPath> getExclusionPatterns();
	
	@Nullable Object getSourceAttachmentPath();
	
	@Nullable IPath getOutputPath();
	
	boolean isExported();
	
	ImList<BuildpathAttribute> getExtraAttributes();
	
	
}
