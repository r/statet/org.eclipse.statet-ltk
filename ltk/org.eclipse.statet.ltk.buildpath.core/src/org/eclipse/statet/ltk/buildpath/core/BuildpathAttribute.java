/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.buildpath.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public interface BuildpathAttribute {
	
	
	String SOURCE_ATTACHMENT= "Source.path"; //$NON-NLS-1$
	
	String FILTER_INCLUSIONS= "Filter.inclusions"; //$NON-NLS-1$
	String FILTER_EXCLUSIONS= "Filter.exclusions"; //$NON-NLS-1$
	
	String OUTPUT= "Output.path"; //$NON-NLS-1$
	
	String EXPORTED= "Exported"; //$NON-NLS-1$
	
	String OPTIONAL= "Optional"; //$NON-NLS-1$
	
	
	String getName();
	
	@Nullable String getValue();
	
}
