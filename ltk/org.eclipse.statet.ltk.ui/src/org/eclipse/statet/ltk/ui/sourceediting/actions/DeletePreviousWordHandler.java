/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.actions;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.BlockTextSelection;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.Region;
import org.eclipse.swt.custom.ST;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;


@NonNullByDefault
public class DeletePreviousWordHandler extends SourceEditorTextHandler {
	
	
	public DeletePreviousWordHandler(final SourceEditor editor) {
		super(editor);
	}
	
	
	@Override
	protected int getTextActionId() {
		return ST.DELETE_WORD_PREVIOUS;
	}
	
	@Override
	protected void exec(final ExecData data) throws BadLocationException {
		if (data.getWidget().getBlockSelection()) {
			final BlockTextSelection blockSelection= (BlockTextSelection) data.getViewer().getSelection();
			if (blockSelection.getEndColumn() > blockSelection.getStartColumn()
					|| data.getCaretDocOffset() == data.getCaretDocLineStartOffset()) {
				data.getWidget().invokeAction(ST.DELETE_PREVIOUS);
				return;
			}
			final int newDocOffset;
			if (blockSelection.getStartColumn() > data.getCaretDocLineInformation().getLength()) {
				if (blockSelection.getStartLine() == blockSelection.getEndLine()) {
					data.getWidget().invokeAction(ST.LINE_END);
					return;
				}
				newDocOffset= data.getCaretDocLineEndOffset();
			}
			else {
				newDocOffset= findPreviousWordOffset(data, data.getCaretDocOffset(), true);
			}
			final int newWidgetOffset= data.toWidgetOffset(newDocOffset);
			if (data.getCaretWidgetOffset() != newWidgetOffset && newWidgetOffset >= 0) {
				expandBlockSelection(data, newWidgetOffset);
				data.getWidget().invokeAction(ST.DELETE_PREVIOUS);
			}
		}
		else if (data.getWidget().getSelectionCount() > 0) {
			data.getWidget().invokeAction(ST.DELETE_PREVIOUS);
		}
		else {
			final int newDocOffset= findPreviousWordOffset(data, data.getCaretDocOffset(), false);
			final IRegion docRegion= new Region(newDocOffset,
					data.getCaretDocOffset() - newDocOffset );
			delete(data, docRegion);
		}
	}
	
}
