/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.folding;

import java.lang.reflect.InvocationTargetException;
import java.util.Set;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.ui.sourceediting.folding.FoldingEditorAddon.FoldingStructureComputationContext;


@NonNullByDefault
public interface FoldingProvider {
	
	
	boolean checkConfig(Set<String> groupIds);
	
	boolean isRestoreStateEnabled();
	
	boolean requiresModel();
	
	void collectRegions(FoldingStructureComputationContext ctx)
			throws InvocationTargetException;
	
}
