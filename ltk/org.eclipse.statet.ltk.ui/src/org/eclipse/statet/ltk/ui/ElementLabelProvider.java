/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui;

import org.eclipse.jface.viewers.StyledString;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.TextStyle;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.LtkModelElement;


/**
 * Label provider for objects of the type {@link LtkModelElement}.
 */
@NonNullByDefault
public interface ElementLabelProvider {
	
	
	static final StyledString.Styler TITLE_STYLER= new StyledString.Styler() {
		@Override
		public void applyStyles(final TextStyle style) {
			((StyleRange)style).fontStyle= SWT.BOLD;
		};
	};
	
	
	default @Nullable Image getImage(final LtkModelElement<?> element) {
		return null;
	}
	
	default @Nullable String getText(final LtkModelElement<?> element) {
		return null;
	}
	
	default @Nullable StyledString getStyledText(final LtkModelElement<?> element) {
		final var text= getText(element);
		return (text != null) ? new StyledString(text) : null;
	}
	
	default int @Nullable [] getStyledTextRegions(final LtkModelElement<?> element,
			final int flags, final int[] regions) {
		return null;
	}
	
//	void decorateStyledText(final StyledString text, final LtkModelElement element);
	
}
