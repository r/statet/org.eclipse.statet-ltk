/*=============================================================================#
 # Copyright (c) 2000, 2022 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.text.source.projection.IProjectionListener;
import org.eclipse.jface.text.source.projection.ProjectionViewer;
import org.eclipse.swt.SWT;
import org.eclipse.ui.actions.ActionGroup;
import org.eclipse.ui.editors.text.IFoldingCommandIds;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.menus.CommandContributionItem;
import org.eclipse.ui.menus.CommandContributionItemParameter;
import org.eclipse.ui.texteditor.ITextEditor;

import org.eclipse.statet.ecommons.ui.actions.HandlerContributionItem;
import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.internal.ltk.ui.EditingMessages;


/**
 * Groups the folding actions.
 */
public class FoldingActionGroup extends ActionGroup implements IProjectionListener {
	
	
	private class ViewerOperationHandler extends AbstractHandler {
		
		private final int operationCode;
		
		public ViewerOperationHandler(final int operationCode) {
			super();
			this.operationCode= operationCode;
		}
		
		public void update() {
			setBaseEnabled(UIAccess.isOkToUse(FoldingActionGroup.this.viewer) && FoldingActionGroup.this.viewer.isProjectionMode());
		}
		
		@Override
		public Object execute(final ExecutionEvent event) throws ExecutionException {
			if (UIAccess.isOkToUse(FoldingActionGroup.this.viewer) && FoldingActionGroup.this.viewer.canDoOperation(this.operationCode)) {
				FoldingActionGroup.this.viewer.doOperation(this.operationCode);
			}
			return null;
		}
		
	}
	
	
	private ITextEditor editor;
	private ProjectionViewer viewer;
	
	private final ViewerOperationHandler expand;
	private final ViewerOperationHandler collapse;
	private final ViewerOperationHandler expandAll;
	private final ViewerOperationHandler collapseAll;
	
	
	/**
	 * Creates a new projection action group for <code>editor</code>.
	 * 
	 * @param editor the text editor to operate on
	 * @param viewer the viewer of the editor
	 */
	public FoldingActionGroup(final ITextEditor editor, final ProjectionViewer viewer) {
		this.editor= editor;
		this.viewer= viewer;
		final IHandlerService handlerService= this.editor.getSite().getService(IHandlerService.class);
		
		this.expandAll= new ViewerOperationHandler(ProjectionViewer.EXPAND_ALL);
		handlerService.activateHandler(IFoldingCommandIds.FOLDING_EXPAND_ALL, this.expandAll);
		
		this.collapseAll= new ViewerOperationHandler(ProjectionViewer.COLLAPSE_ALL); 
		handlerService.activateHandler(IFoldingCommandIds.FOLDING_COLLAPSE_ALL, this.collapseAll);
		
		this.expand= new ViewerOperationHandler(ProjectionViewer.EXPAND); 
		handlerService.activateHandler(IFoldingCommandIds.FOLDING_EXPAND, this.expand);
		
		this.collapse= new ViewerOperationHandler(ProjectionViewer.COLLAPSE); 
		handlerService.activateHandler(IFoldingCommandIds.FOLDING_COLLAPSE, this.collapse);
		
		this.viewer.addProjectionListener(this);
		
		update();
	}
	
	/**
	 * Note: this is not intend to use to remove the actions from the editor
	 */
	@Override
	public void dispose() {
		this.editor= null;
		this.viewer= null;
		super.dispose();
	}
	
	/**
	 * Updates the actions.
	 */
	protected void update() {
		if (this.viewer != null) {
			this.expand.update();
			this.expandAll.update();
			this.collapse.update();
			this.collapseAll.update();
		}
	}
	
	/**
	 * Fills the menu with all folding actions.
	 * 
	 * @param menuManager the menu manager for the folding submenu
	 */
	public void fillMenu(final IMenuManager menuManager) {
		if (this.viewer != null) {
			update();
			final var site= this.editor.getSite();
			
			menuManager.add(new CommandContributionItem(
					new CommandContributionItemParameter(site,
							null, IFoldingCommandIds.FOLDING_TOGGLE, null,
							null, null, null,
							EditingMessages.CodeFolding_Enable_label, EditingMessages.CodeFolding_Enable_mnemonic, null,
							SWT.CHECK, null, false )));
			menuManager.add(new Separator());
			menuManager.add(new HandlerContributionItem(
					new CommandContributionItemParameter(site,
							null, IFoldingCommandIds.FOLDING_EXPAND_ALL, null,
							null, null, null,
							EditingMessages.CodeFolding_ExpandAll_label, EditingMessages.CodeFolding_ExpandAll_mnemonic, null,
							SWT.CHECK, null, false ),
					this.expandAll ));
			menuManager.add(new HandlerContributionItem(
					new CommandContributionItemParameter(site,
							null, IFoldingCommandIds.FOLDING_COLLAPSE_ALL, null,
							null, null, null,
							EditingMessages.CodeFolding_CollapseAll_label, EditingMessages.CodeFolding_CollapseAll_mnemonic, null,
							SWT.CHECK, null, false ),
					this.collapseAll ));
		}
	}
	
	@Override
	public void updateActionBars() {
		update();
	}
	
	@Override
	public void projectionEnabled() {
		update();
	}
	
	@Override
	public void projectionDisabled() {
		update();
	}
	
}
