/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.compare;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.compare.CompareConfiguration;
import org.eclipse.compare.contentmergeviewer.ContentMergeViewer;
import org.eclipse.compare.contentmergeviewer.TextMergeViewer;
import org.eclipse.core.filebuffers.IDocumentSetupParticipant;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.TextViewer;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import org.eclipse.statet.ecommons.preferences.ui.SettingsUpdater;

import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.ltk.ui.sourceediting.ViewerSourceEditorAdapter;


/**
 * {@link ContentMergeViewer} for source code using a {@link SourceEditorViewerConfigurator}
 * to setup the text viewers.
 */
public abstract class CompareMergeTextViewer extends TextMergeViewer {
	
	
	private List<TextViewer> textViewers;
	
	private IDocumentSetupParticipant documentSetupParticipant;
	
	
	public CompareMergeTextViewer(final Composite parent, final CompareConfiguration configuration) {
		super(parent, SWT.LEFT_TO_RIGHT, configuration);
	}
	
	
	@Override
	protected void setupDocument(final IDocument document) {
		if (this.documentSetupParticipant == null) {
			this.documentSetupParticipant= createDocumentSetupParticipant();
		}
		this.documentSetupParticipant.setup(document);
	}
	
	protected abstract IDocumentSetupParticipant createDocumentSetupParticipant();
	protected abstract SourceEditorViewerConfigurator createConfigurator(SourceViewer sourceViewer);
	
	@Override
	protected void configureTextViewer(final TextViewer textViewer) {
		if (textViewer instanceof SourceViewer) {
			final SourceViewer sourceViewer= (SourceViewer) textViewer;
			
			final SourceEditorViewerConfigurator configurator= createConfigurator(sourceViewer);
			configurator.setTarget(new ViewerSourceEditorAdapter(sourceViewer, configurator));
			
			new SettingsUpdater(configurator, sourceViewer.getControl());
		}
		else {
			super.configureTextViewer(textViewer);
		}
		
		if (this.textViewers == null) {
			this.textViewers= new ArrayList<>(3);
		}
		this.textViewers.add(textViewer);
	}
	
}
