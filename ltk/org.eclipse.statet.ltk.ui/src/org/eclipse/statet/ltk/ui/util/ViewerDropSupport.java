/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.util;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.util.DelegatingDropAdapter;
import org.eclipse.jface.util.TransferDropTargetListener;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.swt.dnd.DND;

import org.eclipse.statet.ecommons.ui.util.PluginTransferDropAdapter;

import org.eclipse.statet.ltk.refactoring.core.CommonRefactoringFactory;


public class ViewerDropSupport {
	
	
	private final StructuredViewer viewer;
	private final DelegatingDropAdapter delegatingAdapter;
	
	private final ViewerSelectionTransferDropAdapter reorgDropListener;
	
	private boolean initialized;
	
	
	public ViewerDropSupport(final StructuredViewer viewer, final IAdaptable part,
			final CommonRefactoringFactory refactoring) {
		this.viewer= viewer;
		
		this.delegatingAdapter= new DelegatingDropAdapter();
		this.reorgDropListener= new ViewerSelectionTransferDropAdapter(this.viewer, part, refactoring);
		this.reorgDropListener.setFeedbackEnabled(true);
		this.delegatingAdapter.addDropTargetListener(this.reorgDropListener);
//		this.delegatingAdapter.addDropTargetListener(new FileTransferDropAdapter(this.viewer));
		this.delegatingAdapter.addDropTargetListener(new PluginTransferDropAdapter(this.viewer));
		
		this.initialized= false;
	}
	
	
	public void addDropTargetListener(final TransferDropTargetListener listener) {
		assert (!this.initialized);
		
		this.delegatingAdapter.addDropTargetListener(listener);
	}
	
	public void init() {
		Assert.isLegal(!this.initialized);
		
		this.viewer.addDropSupport((DND.DROP_COPY | DND.DROP_MOVE | DND.DROP_LINK | DND.DROP_DEFAULT),
				this.delegatingAdapter.getTransfers(), this.delegatingAdapter);
		
		this.initialized= true;
	}
	
	public void setFeedbackEnabled(final boolean enabled) {
		this.reorgDropListener.setFeedbackEnabled(enabled);
	}
	
}
