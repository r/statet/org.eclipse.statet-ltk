/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.actions;

import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.dialogs.ListDialog;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.texteditor.ITextEditor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.model.core.element.NameAccess;
import org.eclipse.statet.ltk.model.core.element.SourceElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.WorkspaceSourceUnit;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.util.LTKSelectionUtils;


@NonNullByDefault
public class OpenDeclaration {
	
	
	public OpenDeclaration() {
	}
	
	
	public <T> @Nullable T selectElement(final List<? extends T> list, final IWorkbenchPart part)
			throws CoreException {
		if (list.isEmpty()) {
			return null;
		}
		else if (list.size() == 1) {
			return list.get(0);
		}
		else {
			final ListDialog dialog= new ListDialog(part != null ?
					part.getSite().getShell() :
					UIAccess.getActiveWorkbenchShell(true) );
			dialog.setTitle("Open Declaration");
			dialog.setMessage("Select the appropriate declaration:");
			dialog.setHelpAvailable(false);
			dialog.setContentProvider(new ArrayContentProvider());
			dialog.setLabelProvider(createLabelProvider());
			dialog.setInput(list);
			dialog.setInitialSelections(list.get(0));
			
			if (dialog.open() == Window.OK) {
				return (T)dialog.getResult()[0];
			}
			else {
				throw new CoreException(Status.CANCEL_STATUS);
			}
		}
	}
	
	public <TNameAccess extends NameAccess<?, ?>> @Nullable TNameAccess selectAccess(
			final List<? extends TNameAccess> accessList) {
		for (final TNameAccess candidate : accessList) {
			if (candidate.isWriteAccess()) {
				return candidate;
			}
		}
		return null;
	}
	
	public ILabelProvider createLabelProvider() {
		return new LabelProvider();
	}
	
	public void open(final SourceElement<?> element, final boolean activate)
			throws PartInitException  {
		final SourceUnit su= element.getSourceUnit();
		if (su instanceof WorkspaceSourceUnit) {
			final IResource resource= ((WorkspaceSourceUnit)su).getResource();
			if (resource.getType() == IResource.FILE) {
				open((IFile)resource, activate, element);
				return;
			}
		}
	}
	
	public void open(final IFile file, final boolean activate,
			final @Nullable SourceElement<?> element)
			throws PartInitException  {
		final IWorkbenchPage page= UIAccess.getActiveWorkbenchPage(true);
		final IEditorDescriptor editorDescriptor= IDE.getEditorDescriptor(file, true, true);
		final FileEditorInput input= new FileEditorInput(file);
		IEditorPart editorPart= page.findEditor(input);
		if (editorPart == null || !(editorPart instanceof ITextEditor)) {
			editorPart= page.openEditor(input, editorDescriptor.getId(), activate);
		}
		else if (activate) {
			page.activate(editorPart);
		}
		
		if (element != null) {
			if (editorPart instanceof ITextEditor) {
				final var region= LTKSelectionUtils.getRegionToSelect(element);
				if (region != null) {
					((ITextEditor)editorPart).selectAndReveal(region.getStartOffset(), region.getLength());
				}
			}
		}
	}
	
	public void open(final SourceEditor editor, final NameAccess<?, ?> access) {
		AstNode node= access.getNameNode();
		if (node != null) {
			editor.selectAndReveal(node.getStartOffset(), node.getLength());
			return;
		}
		node= access.getNode();
		editor.selectAndReveal(node.getStartOffset(), 0);
	}
	
}
