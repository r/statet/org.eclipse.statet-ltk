/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.refactoring;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ltk.core.refactoring.Refactoring;
import org.eclipse.ltk.core.refactoring.RefactoringCore;
import org.eclipse.ltk.core.refactoring.participants.ProcessorBasedRefactoring;
import org.eclipse.ltk.core.refactoring.participants.RefactoringProcessor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.progress.IProgressService;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.ecommons.ui.workbench.WorkbenchUIUtils;

import org.eclipse.statet.internal.ltk.ui.refactoring.Messages;
import org.eclipse.statet.ltk.model.core.ModelManager;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.model.core.element.SourceUnitModelInfo;
import org.eclipse.statet.ltk.refactoring.core.CommonRefactoringFactory;
import org.eclipse.statet.ltk.refactoring.core.RefactoringAdapter;
import org.eclipse.statet.ltk.refactoring.core.RefactoringDestination;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;


/**
 * Command handler pasting elements from clipboard at selected position.
 */
public class PasteElementsHandler extends AbstractElementsHandler {
	
	
	private final SourceEditor editor;
	
	
	public PasteElementsHandler(final SourceEditor editor,
			final CommonRefactoringFactory refactoring) {
		super(refactoring);
		this.editor= editor;
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setEnabled(final Object evaluationContext) {
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final ISelection selection= WorkbenchUIUtils.getCurrentSelection(event.getApplicationContext());
		if (selection == null) {
			return null;
		}
		final SourceUnit su= this.editor.getSourceUnit();
		if (su == null) {
			return null;
		}
		RefactoringDestination destination= null;
		if (selection instanceof IStructuredSelection) {
			final IProgressMonitor monitor= new NullProgressMonitor();
			final IStructuredSelection structuredSelection= (IStructuredSelection) selection;
			final SourceUnitModelInfo modelInfo= su.getModelInfo(null, ModelManager.MODEL_FILE,
					monitor );
			if (modelInfo == null) {
				return null;
			}
			if (structuredSelection.isEmpty()) {
				destination= new RefactoringDestination(modelInfo.getSourceElement());
			}
			else if (structuredSelection.size() == 1) {
				final Object object= structuredSelection.getFirstElement();
				destination= new RefactoringDestination(object);
			}
		}
		if (destination == null || !destination.isOK()) {
			return null;
		}
		final RefactoringAdapter adapter= this.refactoring.createAdapter(destination);
		if (adapter == null) {
			return null;
		}
		
		final String code= getCodeFromClipboard(event);
		if (code == null || code.length() == 0) {
			return null;
		}
		
		final IWorkbenchPart activePart= HandlerUtil.getActivePart(event);
		final IWorkbenchPartSite site= activePart.getSite();
		final Shell shell= site.getShell();
		final IProgressService progressService= site.getService(IProgressService.class);
		
		try {
			final Position position= startInsertRefactoring(code, destination, su, adapter, shell, progressService);
			if (position != null && !position.isDeleted()) {
				this.editor.selectAndReveal(position.getOffset(), 0);
			}
		}
		catch (final InvocationTargetException e) {
			StatusManager.getManager().handle(new Status(
					IStatus.ERROR, adapter.getPluginIdentifier(), -1,
					Messages.PastingElements_error_message,
					e.getCause() ),
					StatusManager.LOG | StatusManager.SHOW);
		}
		catch (final InterruptedException e) {
		}
		return null;
	}
	
	private Position startInsertRefactoring(final String code, final RefactoringDestination destination,
			final SourceUnit su, final RefactoringAdapter adapter,
			final Shell shell, final IProgressService context)
			throws InvocationTargetException, InterruptedException {
		final RefactoringProcessor processor= this.refactoring.createPasteProcessor(code, destination, adapter);
		final Refactoring refactoring= new ProcessorBasedRefactoring(processor);
		final RefactoringExecutionHelper helper= new RefactoringExecutionHelper(refactoring, 
				RefactoringCore.getConditionCheckingFailedSeverity(), 
				shell, context );
		helper.enableInsertPosition(su);
		helper.perform(false, false);
		
		return helper.getInsertPosition();
	}
	
}
