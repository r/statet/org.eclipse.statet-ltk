/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.templates;

import java.util.Iterator;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DefaultLineTracker;
import org.eclipse.jface.text.ILineTracker;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.text.templates.TemplateBuffer;
import org.eclipse.jface.text.templates.TemplateContext;
import org.eclipse.jface.text.templates.TemplateContextType;
import org.eclipse.jface.text.templates.TemplateException;
import org.eclipse.jface.text.templates.TemplateTranslator;
import org.eclipse.jface.text.templates.TemplateVariableResolver;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor1;


@NonNullByDefault
public class CodeGenerationTemplateContext extends TemplateContext implements IWorkbenchTemplateContext {
	
	
	private final String lineDelimiter;
	
	private @Nullable SourceUnit sourceUnit;
	
	
	public CodeGenerationTemplateContext(final TemplateContextType contextType,
			final String lineDelim) {
		super(contextType);
		this.lineDelimiter= lineDelim;
	}
	
	public CodeGenerationTemplateContext(final TemplateContextType contextType,
			final SourceUnit sourceUnit, final String lineDelim) {
		this(contextType, lineDelim);
		this.sourceUnit= sourceUnit;
	}
	
	
	@Override
	public @Nullable SourceEditor1 getEditor() {
		return null;
	}
	
	@Override
	public @Nullable SourceUnit getSourceUnit() {
		return this.sourceUnit;
	}
	
	@Override
	public @Nullable String evaluateInfo(final Template template)
			throws BadLocationException, TemplateException {
		final TemplateBuffer buffer= evaluate(template);
		if (buffer != null) {
			buffer.getString();
		}
		return null;
	}
	
	@Override
	public boolean canEvaluate(final Template template) {
		return true;
	}
	
	@Override
	public @Nullable TemplateBuffer evaluate(final Template template)
			throws BadLocationException, TemplateException {
		// test that all variables are defined
		final Iterator<TemplateVariableResolver> iterator= getContextType().resolvers();
		while (iterator.hasNext()) {
			final TemplateVariableResolver var= iterator.next();
			if (var instanceof SourceEditorContextType.CodeTemplatesVariableResolver) {
				Assert.isNotNull(getVariable(var.getType()), "Variable " + var.getType() + "not defined"); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
		
		if (!canEvaluate(template)) {
			return null;
		}
		
		final String pattern= changeLineDelimiter(template.getPattern(), this.lineDelimiter);
		
		final TemplateTranslator translator= new TemplateTranslator();
		final TemplateBuffer buffer= translator.translate(pattern);
		if (buffer == null) {
			// translator.getErrorMessage();
			return null;
		}
		getContextType().resolve(buffer, this);
		return buffer;
	}
	
	private static String changeLineDelimiter(final String code, final String lineDelim) {
		try {
			final ILineTracker tracker= new DefaultLineTracker();
			tracker.set(code);
			final int nLines= tracker.getNumberOfLines();
			if (nLines == 1) {
				return code;
			}
			
			final StringBuffer buf= new StringBuffer();
			for (int i= 0; i < nLines; i++) {
				if (i != 0) {
					buf.append(lineDelim);
				}
				final IRegion region= tracker.getLineInformation(i);
				final String line= code.substring(region.getOffset(), region.getOffset() + region.getLength());
				buf.append(line);
			}
			return buf.toString();
		}
		catch (final BadLocationException e) {
			// can not happen
			return code;
		}
	}
	
}
