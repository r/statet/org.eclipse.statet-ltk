/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.templates;

import java.util.Map;
import java.util.Set;

import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.text.templates.TemplateContextType;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.text.templates.ContextTypeRegistry;
import org.eclipse.ui.editors.text.EditorsUI;

import org.eclipse.statet.ecommons.preferences.ui.SettingsUpdater;
import org.eclipse.statet.ecommons.templates.TemplateVariableProcessor;
import org.eclipse.statet.ecommons.text.ui.TextViewerJFaceUpdater;
import org.eclipse.statet.ecommons.ui.ISettingsChangedHandler;
import org.eclipse.statet.ecommons.ui.util.UIAccess;

import org.eclipse.statet.internal.ltk.ui.PreviewSourceViewer;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.ltk.ui.sourceediting.ViewerSourceEditorAdapter;


public class TemplatePreview {
	
	
	private final TemplateVariableProcessor templateProcessor;
	
	private SourceViewer viewer;
	
	private TextViewerJFaceUpdater viewerUpdater= null;
	private SourceEditorViewerConfigurator configurator;
	private SourceEditor editor;
	
	
	public TemplatePreview() {
		this.templateProcessor= new TemplateVariableProcessor();
	}
	
	
	public TemplateVariableProcessor getTemplateVariableProcessor() {
		return this.templateProcessor;
	}
	
	public SourceViewer createSourceViewer(final Composite parent) {
		final SourceViewer viewer= new PreviewSourceViewer(parent, false,
				EditorsUI.getPreferenceStore() );
		
		final IDocument document= new Document();
		viewer.setDocument(document);
		
		new SettingsUpdater(new ISettingsChangedHandler() {
			@Override
			public void handleSettingsChanged(final Set<String> groupIds, final Map<String, Object> options) {
				if (TemplatePreview.this.configurator != null) {
					TemplatePreview.this.configurator.handleSettingsChanged(groupIds, options);
				}
			}
		}, viewer.getControl());
		
		return this.viewer= viewer;
	}
	
	public void updateSourceViewerInput(final Template template,
			final ContextTypeRegistry contextRegistry,
			final SourceEditorViewerConfigurator patternConfigurator) {
		if (this.viewer == null || !(UIAccess.isOkToUse(this.viewer.getControl())) ) {
			return;
		}
		
		if (template != null) {
			final TemplateContextType type= contextRegistry.getContextType(template.getContextTypeId());
			this.templateProcessor.setContextType(type);
			
			if (patternConfigurator != null && patternConfigurator != this.configurator) {
				if (this.viewerUpdater != null) {
					this.viewerUpdater.dispose();
					this.viewerUpdater= null;
				}
				if (this.configurator != null) {
					this.configurator.unconfigureTarget();
					this.configurator= null;
				}
				
				this.configurator= patternConfigurator;
				this.editor= new ViewerSourceEditorAdapter(this.viewer, this.configurator);
				this.configurator.setTarget(this.editor);
				this.viewerUpdater= new TextViewerJFaceUpdater(this.viewer,
						this.configurator.getSourceViewerConfiguration().getPreferences() );
				
				final IDocument document= new Document(template.getPattern());
				this.configurator.getDocumentSetupParticipant().setup(document);
				this.viewer.setDocument(document);
			}
			else {
				this.viewer.getDocument().set(template.getPattern());
			}
			
		}
		else {
			this.viewer.getDocument().set(""); //$NON-NLS-1$
		}
	}
	
}
