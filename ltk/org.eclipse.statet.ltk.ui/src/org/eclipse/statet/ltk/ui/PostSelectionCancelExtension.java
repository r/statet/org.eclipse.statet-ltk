/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui;

import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentListener;
import org.eclipse.jface.text.ITextInputListener;


/**
 * Extension for {@link PostSelectionWithElementInfoController} for source viewers / documents
 * enabling suppression of selection notification if the document or its content changed.
 */
public abstract class PostSelectionCancelExtension implements ITextInputListener, IDocumentListener {
	
	
	PostSelectionWithElementInfoController controller;
	
	
	public abstract void init();
	public abstract void dispose();
	
	
	@Override
	public void inputDocumentAboutToBeChanged(final IDocument oldInput, final IDocument newInput) {
		if (oldInput != null) {
			oldInput.removeDocumentListener(this);
		}
		this.controller.cancel();
	}
	
	@Override
	public void inputDocumentChanged(final IDocument oldInput, final IDocument newInput) {
		if (newInput != null) {
			newInput.addDocumentListener(this);
		}
	}
	
	@Override
	public void documentAboutToBeChanged(final DocumentEvent event) {
		this.controller.cancel();
	}
	
	@Override
	public void documentChanged(final DocumentEvent event) {
	}
	
}
