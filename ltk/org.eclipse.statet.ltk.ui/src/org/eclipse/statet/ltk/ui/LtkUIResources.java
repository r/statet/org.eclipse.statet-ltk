/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.util.UIResources;

import org.eclipse.statet.internal.ltk.ui.LtkUIPlugin;


@NonNullByDefault
public class LtkUIResources extends UIResources {
	
	
	private static final String NS= LtkUI.BUNDLE_ID;
	
	
	public static final String OBJ_ERROR_IMAGE_ID= NS + "/images/obj/Error"; //$NON-NLS-1$
	public static final String OBJ_ERROR_AWAY_IMAGE_ID= NS + "/images/obj/Error.away"; //$NON-NLS-1$
	public static final String OBJ_WARNING_IMAGE_ID= NS + "/images/obj/Warning"; //$NON-NLS-1$
	public static final String OBJ_WARNING_AWAY_IMAGE_ID= NS + "/images/obj/Warning.away"; //$NON-NLS-1$
	public static final String OBJ_INFO_IMAGE_ID= NS + "/images/obj/Info"; //$NON-NLS-1$
	public static final String OBJ_INFO_AWAY_IMAGE_ID= NS + "/images/obj/Info.away"; //$NON-NLS-1$
	
	public static final String OBJ_TEXT_TEMPLATE_IMAGE_ID= NS + "/images/obj/text-template"; //$NON-NLS-1$
	public static final String OBJ_TEXT_AT_TAG_IMAGE_ID= NS + "/images/obj/text-at_tag"; //$NON-NLS-1$
	
	public static final String OBJ_TEXT_LINKEDRENAME_IMAGE_ID= NS + "/images/tool/assist-linked_rename"; //$NON-NLS-1$
	
	
	static final LtkUIResources INSTANCE= new LtkUIResources();
	
	
	private LtkUIResources() {
		super(LtkUIPlugin.getInstance().getImageRegistry());
	}
	
	
}
