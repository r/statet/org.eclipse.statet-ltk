/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.text.IRegion;
import org.eclipse.ui.texteditor.IUpdate;


/**
 * Action to restore the last structure selection.
 * 
 * @see StructureSelectHandler
 */
public class StructureSelectionHistoryBackHandler extends AbstractHandler implements IUpdate {
	
	
	private final SourceEditor sourceEditor;
	
	private final StructureSelectionHistory history;
	
	
	public StructureSelectionHistoryBackHandler(final SourceEditor editor, final StructureSelectionHistory history) {
		super();
		assert (editor != null);
		assert (history != null);
		this.sourceEditor= editor;
		this.history= history;
		update();
	}
	
	
	@Override
	public void update() {
		setBaseEnabled(!this.history.isEmpty());
	}
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		final IRegion old= this.history.getLast();
		if (old != null) {
			try {
				this.history.ignoreSelectionChanges();
				this.sourceEditor.selectAndReveal(old.getOffset(), old.getLength());
			}
			finally {
				this.history.listenToSelectionChanges();
			}
		}
		return null;
	}
	
}
