/*=============================================================================#
 # Copyright (c) 2017, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting;

import static org.eclipse.ui.texteditor.ITextEditorActionDefinitionIds.SHOW_WHITESPACE_CHARACTERS;
import static org.eclipse.ui.texteditor.ITextEditorActionDefinitionIds.WORD_WRAP;

import static org.eclipse.statet.ecommons.ui.actions.UIActions.VIEW_GROUP_ID;

import org.eclipse.core.commands.IHandler2;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.swt.SWT;
import org.eclipse.ui.menus.CommandContributionItemParameter;
import org.eclipse.ui.services.IServiceLocator;

import org.eclipse.statet.ecommons.commands.core.HandlerCollection;
import org.eclipse.statet.ecommons.preferences.core.Preference.BooleanPref;
import org.eclipse.statet.ecommons.ui.actions.HandlerContributionItem;

import org.eclipse.statet.internal.ltk.ui.EditingMessages;
import org.eclipse.statet.ltk.ui.sourceediting.actions.ToggleShowWhitespaceHandler;
import org.eclipse.statet.ltk.ui.sourceediting.actions.ToggleSoftWordWrapHandler;


public class SnippetEditor1 extends SnippetEditor {
	
	
	private static final String SHOW_WHITESPACE_PREF_KEY= "ShowWhitespace.enabled"; //$NON-NLS-1$
	private static final String SOFT_WORD_WRAP_PREF_KEY= "SoftWordWrap.enabled"; //$NON-NLS-1$
	
	
	private final String prefQualifier;
	
	
	public SnippetEditor1(final SourceEditorViewerConfigurator configurator,
			final String initialContent,
			final IServiceLocator serviceParent, final String prefQualifier,
			final boolean withToolButton) {
		super(configurator, initialContent, serviceParent, withToolButton);
		
		this.prefQualifier= prefQualifier;
	}
	
	public SnippetEditor1(final SourceEditorViewerConfigurator configurator,
			final String initialContent,
			final IServiceLocator serviceParent, final String prefQualifier) {
		this(configurator, initialContent, serviceParent, prefQualifier, false);
	}
	
	
	@Override
	protected void initActions() {
		super.initActions();
		
		if (this.prefQualifier != null) {
			registerCommandHandler(SHOW_WHITESPACE_CHARACTERS, new ToggleShowWhitespaceHandler(
							new BooleanPref(this.prefQualifier, SHOW_WHITESPACE_PREF_KEY),
							getSourceViewer() ));
			if ((getSourceViewer().getTextWidget().getStyle() & SWT.MULTI) != 0) {
				registerCommandHandler(WORD_WRAP, new ToggleSoftWordWrapHandler(
						new BooleanPref(this.prefQualifier, SOFT_WORD_WRAP_PREF_KEY),
						getSourceViewer() ));
			}
		}
	}
	
	
	@Override
	protected void fillContextMenu(final IMenuManager menu) {
		super.fillContextMenu(menu);
		
		final IServiceLocator serviceLocator= getServiceLocator();
		final HandlerCollection handlers= getCommandHandlers();
		
		{	final IHandler2 handler= handlers.get(SHOW_WHITESPACE_CHARACTERS);
			if (handler != null) {
				menu.appendToGroup(VIEW_GROUP_ID, new HandlerContributionItem(new CommandContributionItemParameter(
						serviceLocator, SHOW_WHITESPACE_CHARACTERS, SHOW_WHITESPACE_CHARACTERS, null,
						null, null, null,
						EditingMessages.EditorMenu_ToggleShowWhitespace_label, null, null,
						HandlerContributionItem.STYLE_CHECK, null, false ), handler ));
			}
		}
		{	final IHandler2 handler= handlers.get(WORD_WRAP);
			if (handler != null) {
				menu.appendToGroup(VIEW_GROUP_ID, new HandlerContributionItem(new CommandContributionItemParameter(
						serviceLocator, WORD_WRAP, WORD_WRAP, null,
						null, null, null,
						EditingMessages.EditorMenu_ToggleSoftWordWrap_label, null, null,
						HandlerContributionItem.STYLE_CHECK, null, false ), handler ));
			}
		}
	}
	
}
