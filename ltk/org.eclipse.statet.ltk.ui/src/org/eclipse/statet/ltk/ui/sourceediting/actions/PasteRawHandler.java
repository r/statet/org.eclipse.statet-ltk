/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.actions;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.text.ITextOperationTarget;
import org.eclipse.ui.texteditor.ITextEditorExtension3;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ui.sourceediting.SourceEditor;


@NonNullByDefault
public class PasteRawHandler extends SourceEditorOperationHandler {
	
	
	public PasteRawHandler() {
		super(ITextOperationTarget.PASTE);
	}
	
	
	@Override
	protected @Nullable Object execute(final SourceEditor editor, final ExecutionEvent event)
			throws ExecutionException {
		final ITextEditorExtension3 editor3;
		if (editor instanceof ITextEditorExtension3
				&& ((editor3= (ITextEditorExtension3)editor).getInsertMode() == ITextEditorExtension3.SMART_INSERT) ) {
			editor3.setInsertMode(ITextEditorExtension3.INSERT);
			try {
				return super.execute(editor, event);
			}
			finally {
				editor3.setInsertMode(ITextEditorExtension3.SMART_INSERT);
			}
		}
		else {
			return super.execute(editor, event);
		}
	}
	
}
