/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui;

import java.util.Comparator;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;

import org.eclipse.statet.ltk.core.ElementName;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;


public class ElementNameComparator extends ViewerComparator implements Comparator<LtkModelElement> {
	
	
	public ElementNameComparator(final Comparator<? super ElementName> nameComparator) {
		super((Comparator) nameComparator);
	}
	
	
	@Override
	public int category(final Object element) {
		return category((LtkModelElement) element);
	}
	
	public int category(final LtkModelElement element) {
		switch (element.getElementType() & LtkModelElement.MASK_C1) {
		case LtkModelElement.C1_IMPORT:
			return 1;
		case LtkModelElement.C1_CLASS:
			return 2;
		}
		return 100;
	}
	
	@Override
	public int compare(final LtkModelElement e1, final LtkModelElement e2) {
		final int c1= category(e1);
		final int c2= category(e2);
		if (c1 != c2) {
			return c1 - c2;
		}
		final int result= ((Comparator<? super ElementName>) getComparator())
				.compare(e1.getElementName(), e2.getElementName());
		if (result != 0) {
			return result;
		}
		if (e1 instanceof SourceStructElement && e2 instanceof SourceStructElement) {
			return ((SourceStructElement) e1).getSourceRange().getStartOffset()
					- ((SourceStructElement) e2).getSourceRange().getStartOffset();
		}
		return 0;
	}
	
	@Override
	public int compare(final Viewer viewer, final Object e1, final Object e2) {
		if (e1 instanceof LtkModelElement) {
			if (e2 instanceof LtkModelElement) {
				return compare((LtkModelElement) e1, (LtkModelElement) e2);
			}
			else {
				return Integer.MIN_VALUE;
			}
		}
		else {
			if (e2 instanceof LtkModelElement) {
				return Integer.MAX_VALUE;
			}
			else {
				return 0;
			}
		}
	}
	
}
