/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.assist;

import java.util.Objects;

import org.eclipse.jface.text.contentassist.BoldStylerProvider;
import org.eclipse.jface.text.contentassist.ICompletionProposalExtension3;
import org.eclipse.jface.text.contentassist.ICompletionProposalExtension6;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.swt.graphics.Image;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.viewers.ViewerLabelUtils;

import org.eclipse.statet.ltk.core.ElementName;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.ui.ElementLabelProvider;


/**
 * Proposal completing a given {@link ElementName} of a element.
 */
@NonNullByDefault
public abstract class ElementNameCompletionProposal<
				TContext extends AssistInvocationContext, TElement extends LtkModelElement>
		extends SourceProposal<TContext>
		implements ICompletionProposalExtension3, ICompletionProposalExtension6 {
	
	
	private final ElementName replacementName;
	
	private final @Nullable TElement element;
	
	private final ElementLabelProvider labelProvider;
	
	
	public ElementNameCompletionProposal(final ProposalParameters<? extends TContext> parameters,
			final ElementName replacementName, final @Nullable TElement element,
			final ElementLabelProvider labelProvider) {
		super(parameters);
		
		this.replacementName= replacementName;
		this.element= element;
		this.labelProvider= labelProvider;
	}
	
	
	public final @Nullable TElement getElement() {
		return this.element;
	}
	
	public ElementName getReplacementName() {
		return this.replacementName;
	}
	
	@Override
	protected String getName() {
		return getReplacementName().getDisplayName();
	}
	
	@Override
	protected String getValidationName() {
		return getReplacementName().getSegmentName();
	}
	
	
	@Override
	public String getSortingString() {
		return getReplacementName().getSegmentName();
	}
	
	
	//-- Item Label --
	
	protected final ElementLabelProvider getLabelProvider() {
		return this.labelProvider;
	}
	
	@Override
	public String getDisplayString() {
		final var element= this.element;
		return (element != null) ?
				this.labelProvider.getText(element) :
				getReplacementName().getDisplayName();
	}
	
	@Override
	public StyledString computeStyledText() {
		final var element= this.element;
		return (element != null) ?
				this.labelProvider.getStyledText(element) :
				new StyledString(getDisplayString());
	}
	
	@Override
	protected void styleMatchingRegions(final StyledString styledText,
			final int matchRule, int[] matchingRegions,
			final BoldStylerProvider boldStylerProvider) {
		final var element= this.element;
		matchingRegions= (element != null) ?
				this.labelProvider.getStyledTextRegions(element, 0, matchingRegions) :
				getReplacementName().correctDisplayNameRegions(matchingRegions, 0);
		ViewerLabelUtils.setStyle(styledText, matchingRegions, boldStylerProvider.getBoldStyler());
	}
	
	
	@Override
	public Image getImage() {
		final var element= this.element;
		return (element != null) ?
				this.labelProvider.getImage(element) :
				super.getImage();
	}
	
	
	@Override
	public int hashCode() {
		return getClass().hashCode() * Objects.hashCode(getReplacementName());
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj != null && getClass() == obj.getClass()) {
			final ElementNameCompletionProposal<?, ?> other= (ElementNameCompletionProposal<?, ?>) obj;
			return (Objects.equals(getReplacementName(), other.getReplacementName())
					&& Objects.equals(getElement(), other.getElement()) );
		}
		return false;
	}
	
}
