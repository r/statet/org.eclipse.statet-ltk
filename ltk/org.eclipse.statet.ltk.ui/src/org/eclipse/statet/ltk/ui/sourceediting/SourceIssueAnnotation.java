/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting;

import org.eclipse.jface.text.source.Annotation;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.issues.core.Issue;
import org.eclipse.statet.ltk.issues.core.IssueTypeSet;


@NonNullByDefault
public interface SourceIssueAnnotation<TIssue extends Issue> {
	
	
	/**
	 * Returns the category of the issue presented by this annotation.
	 * 
	 * @return the issue category.
	 */
	IssueTypeSet.IssueCategory<TIssue> getIssueCategory();
	
	/**
	 * Returns the annotation type.
	 * 
	 * @return the type.
	 */
	String getType();
	
	boolean isMarkedDeleted();
	
	
	/**
	 * Returns the annotation overlaying this annotation.
	 * 
	 * @return the overlay annotation if overlaid, otherwise {@code null}.
	 */
	@Nullable SourceIssueAnnotation<TIssue> getOverlay();
	
	ImList<Annotation> getOverlaidAnnotations();
	
}
