/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.util;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullElse;

import java.util.IdentityHashMap;

import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.texteditor.IDocumentProvider;

import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.text.core.TextRegion;

import org.eclipse.statet.ecommons.ui.SharedUIResources;
import org.eclipse.statet.ecommons.ui.jface.resource.ImageImageDescriptor;

import org.eclipse.statet.ltk.ast.core.AstNode;
import org.eclipse.statet.ltk.ast.core.Asts;
import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.issues.core.Issue;
import org.eclipse.statet.ltk.issues.core.Problem;
import org.eclipse.statet.ltk.model.core.LtkModelUtils;
import org.eclipse.statet.ltk.model.core.LtkModels;
import org.eclipse.statet.ltk.model.core.element.EmbeddingForeignElement;
import org.eclipse.statet.ltk.model.core.element.LtkModelElement;
import org.eclipse.statet.ltk.model.core.element.SourceElement;
import org.eclipse.statet.ltk.model.core.element.SourceStructElement;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;
import org.eclipse.statet.ltk.ui.ElementLabelProvider;
import org.eclipse.statet.ltk.ui.sourceediting.SourceIssueEditorAnnotation;


@NonNullByDefault
public class ExtModelLabelProvider extends StyledCellLabelProvider
		implements ILabelProvider, ElementLabelProvider, Disposable {
	
	
	protected static final ElementLabelProvider NO_PROVIDER= new ElementLabelProvider() {
	};
	
	
	private final IdentityHashMap<String, ElementLabelProvider> providers= new IdentityHashMap<>(4);
	
	private @Nullable String modelTypeId;
	
	private @Nullable String documentProviderModelTypeId;
	private @Nullable IDocumentProvider documentProvider;
	
	private final SharedUIResources sharedResources= SharedUIResources.getInstance();
	
	
	public ExtModelLabelProvider(final String modelTypeId) {
		this.modelTypeId= modelTypeId;
	}
	
	public ExtModelLabelProvider() {
	}
	
	
	@Override
	public void dispose() {
		for (final var provider : this.providers.values()) {
			if (provider instanceof Disposable) {
				((Disposable)provider).dispose();
			}
		}
		this.providers.clear();
		
		super.dispose();
	}
	
	
	protected @Nullable ElementLabelProvider getElementLabelProvider(final @Nullable String modelId) {
		if (modelId == null) {
			return null;
		}
		var provider= this.providers.get(modelId);
		if (provider == null) {
			provider= LtkModels.getModelAdapter(modelId, ElementLabelProvider.class);
			if (provider == null) {
				provider= NO_PROVIDER;
			}
			this.providers.put(modelId, provider);
		}
		return (provider != NO_PROVIDER) ? provider : null;
	}
	
	protected void addProvider(final String modelId, final ElementLabelProvider provider) {
		this.providers.put(modelId, provider);
	}
	
	
	@Override
	public @Nullable Image getImage(final @Nullable Object element) {
		if (element instanceof LtkModelElement) {
			return getImage((LtkModelElement<?>)element);
		}
		return null;
	}
	
	@Override
	public @Nullable String getText(final @Nullable Object element) {
		if (element instanceof LtkModelElement) {
			return getText((LtkModelElement<?>)element);
		}
		return null;
	}
	
	
	@Override
	public @Nullable Image getImage(final LtkModelElement<?> element) {
		final boolean embedding= ((element.getElementType() & LtkModelElement.MASK_C1) == LtkModelElement.C1_EMBEDDED);
		Image image= null;
		ElementLabelProvider provider;
		if (image == null && embedding && this.modelTypeId != element.getModelTypeId()
				&& (provider= getElementLabelProvider(this.modelTypeId)) != null) {
			image= provider.getImage(element);
		}
		if (image == null
				&& (provider= getElementLabelProvider(element.getModelTypeId())) != null) {
			image= provider.getImage(element);
		}
		if (image == null && embedding) {
			final var embeddedElement= ((EmbeddingForeignElement<?, ?>)element).getForeignElement();
			if (embeddedElement != null
					&& (provider= getElementLabelProvider(embeddedElement.getModelTypeId())) != null) {
				image= provider.getImage(embeddedElement);
			}
		}
		if (image != null) {
			final int adornmentFlags= computeAdornmentFlags(element);
			if (adornmentFlags != 0) {
				final var imageDescriptor= new DecoratedElementImageDescriptor(
						new ImageImageDescriptor(image), adornmentFlags );
				return this.sharedResources.getImageDescriptorRegistry()
						.get(imageDescriptor);
			}
		}
		return image;
	}
	
	@Override
	public String getText(final LtkModelElement<?> element) {
		final boolean embedding= ((element.getElementType() & LtkModelElement.MASK_C1) == LtkModelElement.C1_EMBEDDED);
		String text= null;
		ElementLabelProvider provider;
		if (text == null && embedding && this.modelTypeId != element.getModelTypeId()
				&& (provider= getElementLabelProvider(this.modelTypeId)) != null) {
			text= provider.getText(element);
		}
		if (text == null
				&& (provider= getElementLabelProvider(element.getModelTypeId())) != null) {
			text= provider.getText(element);
		}
		if (text == null && embedding) {
			final var embeddedElement= ((EmbeddingForeignElement<?, ?>)element).getForeignElement();
			if (embeddedElement != null
					&& (provider= getElementLabelProvider(embeddedElement.getModelTypeId())) != null) {
				text= provider.getText(embeddedElement);
			}
		}
		if (text == null) {
			text= element.getElementName().getDisplayName();
		}
		return text;
	}
	
	@Override
	public StyledString getStyledText(final LtkModelElement<?> element) {
		final boolean embedding= ((element.getElementType() & LtkModelElement.MASK_C1) == LtkModelElement.C1_EMBEDDED);
		StyledString text= null;
		ElementLabelProvider provider;
		if (text == null && embedding && this.modelTypeId != element.getModelTypeId()
				&& (provider= getElementLabelProvider(this.modelTypeId)) != null) {
			text= provider.getStyledText(element);
		}
		if (text == null
				&& (provider= getElementLabelProvider(element.getModelTypeId())) != null) {
			text= provider.getStyledText(element);
		}
		if (text == null && embedding) {
			final var embeddedElement= ((EmbeddingForeignElement<?, ?>)element).getForeignElement();
			if (embeddedElement != null
					&& (provider= getElementLabelProvider(embeddedElement.getModelTypeId())) != null) {
				text= provider.getStyledText(embeddedElement);
			}
		}
		if (text == null) {
			text= new StyledString(element.getElementName().getDisplayName());
		}
		return text;
	}
	
	
	protected int computeAdornmentFlags(final LtkModelElement<?> element) {
		int flags= 0;
		if (element instanceof SourceElement) {
			final var sourceElement= (SourceElement<?>)element;
			final var sourceUnit= sourceElement.getSourceUnit();
			if (sourceUnit != null) {
				if (sourceUnit.getWorkingContext() == Ltk.EDITOR_CONTEXT) {
					if (hasAstError(sourceElement)) {
						flags|= DecoratedElementImageDescriptor.ERROR;
					}
					else if (isUpToDate(sourceElement)) {
						final IAnnotationModel annotationModel= getAnnotationModel(sourceUnit);
						if (annotationModel != null) {
							flags|= getProblemFlag(annotationModel, sourceElement);
						}
					}
				}
			}
		}
		return flags;
	}
	
	private @Nullable IAnnotationModel getAnnotationModel(final SourceUnit sourceUnit) {
		final String modelTypeId= nonNullElse(this.modelTypeId, sourceUnit.getModelTypeId());
		final IDocumentProvider documentProvider;
		if (modelTypeId == this.documentProviderModelTypeId) {
			documentProvider= this.documentProvider;
		}
		else {
			documentProvider= LtkModels.getModelAdapter(modelTypeId, IDocumentProvider.class);
			this.documentProviderModelTypeId= modelTypeId;
			this.documentProvider= documentProvider;
		}
		if (documentProvider == null) {
			return null;
		}
		return documentProvider.getAnnotationModel(sourceUnit);
	}
	
	private boolean isUpToDate(final SourceElement<?> element) {
		if (element instanceof SourceStructElement) {
			final var containerElement= LtkModelUtils.getSourceContainerElement(
					(SourceStructElement<?, ?>)element);
			if (containerElement != null) {
				final var sourceUnit= element.getSourceUnit();
				return (containerElement.getStamp().getContentStamp() == sourceUnit.getContentStamp(null));
			}
		}
		return true;
	}
	
	private boolean hasAstError(final SourceElement<?> element) {
		final boolean embedding= ((element.getElementType() & LtkModelElement.MASK_C1) == LtkModelElement.C1_EMBEDDED);
		AstNode astNode;
		if ((astNode= element.getAdapter(AstNode.class)) != null
				&& Asts.hasErrors(astNode) ) {
			return true;
		}
		if (embedding) {
			final var embeddedElement= ((EmbeddingForeignElement<?, ?>)element).getForeignElement();
			if (embeddedElement != null && (astNode= embeddedElement.getAdapter(AstNode.class)) != null
					&& Asts.hasErrors(astNode) ) {
				return true;
			}
		}
		return false;
	}
	
	private int getProblemFlag(final IAnnotationModel model, final SourceElement<?> sourceElement) {
		int severity= -1;
		for (final var iter= model.getAnnotationIterator();
				(severity != Problem.SEVERITY_ERROR && iter.hasNext()); ) {
			final var annotation= iter.next();
			if (annotation instanceof SourceIssueEditorAnnotation) {
				final var issueAnnotation= (SourceIssueEditorAnnotation)annotation;
				if (!issueAnnotation.isMarkedDeleted()) {
					final Issue issue= issueAnnotation.getIssue();
					if (issue instanceof Problem) {
						if (isInside(model.getPosition(annotation), sourceElement)) {
							severity= Math.max(severity, ((Problem)issue).getSeverity());
						}
					}
				}
				continue;
			}
		}
		switch (severity) {
		case Problem.SEVERITY_INFO:
			return DecoratedElementImageDescriptor.INFO;
		case Problem.SEVERITY_WARNING:
			return DecoratedElementImageDescriptor.WARNING;
		case Problem.SEVERITY_ERROR:
			return DecoratedElementImageDescriptor.ERROR;
		default:
			return 0;
		}
	}
	
	private boolean isInside(final @Nullable Position position,
			final SourceElement<?> sourceElement) {
		return (position != null
				&& (isInside(position, sourceElement.getSourceRange())
						|| isInside(position, sourceElement.getDocumentationRange()) ));
	}
	
	private boolean isInside(final Position position, final @Nullable TextRegion region) {
		return (region != null && region.contains(position.getOffset()));
	}
	
	
	@Override
	public void update(final ViewerCell cell) {
		final Object cellElement= cell.getElement();
		if (cellElement instanceof LtkModelElement) {
			final var element= (LtkModelElement<?>)cellElement;
			cell.setImage(getImage(element));
			final StyledString styledText= getStyledText(element);
			cell.setText(styledText.getString());
			cell.setStyleRanges(styledText.getStyleRanges());
		}
		else {
			cell.setImage(null);
			cell.setText(cellElement.toString());
			cell.setStyleRanges(null);
		}
		super.update(cell);
	}
	
}
