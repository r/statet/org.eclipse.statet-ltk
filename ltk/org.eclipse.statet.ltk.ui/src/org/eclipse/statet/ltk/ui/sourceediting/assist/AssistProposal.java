/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting.assist;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.ICompletionProposalExtension2;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public interface AssistProposal extends ICompletionProposal, ICompletionProposalExtension2 {
	
	
	String QUALIFIER_SEPARATOR= "\u2002–\u2002"; //$NON-NLS-1$
	
	
	/**
	 * Returns relevance of the proposal
	 * <p>
	 * Higher values indicates that it is likely more relevant.</p>
	 * 
	 * @return the relevance
	 */
	int getRelevance();
	
	/**
	 * Returns the string to use when sorting proposals
	 * 
	 * @return the sorting string
	 */
	String getSortingString();
	
//	@Override
//	String getDisplayString();
//	
//	@Override
//	@Nullable Image getImage();
	
	
	@Override
	default void selected(final ITextViewer textViewer, final boolean smartToggle) {
	}
	
	@Override
	default void unselected(final ITextViewer textViewer) {
	}
	
	
	/**
	 * Not supported, use {@link #apply(ITextViewer, char, int, int)}
	 */
	@Override
	@Deprecated
	default void apply(final IDocument document) {
	}
	
}
