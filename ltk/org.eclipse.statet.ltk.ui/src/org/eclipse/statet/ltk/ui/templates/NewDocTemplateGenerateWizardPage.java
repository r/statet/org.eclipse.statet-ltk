/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.templates;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.text.templates.ContextTypeRegistry;
import org.eclipse.text.templates.TemplatePersistenceData;

import org.eclipse.statet.ecommons.preferences.core.EPreferences;
import org.eclipse.statet.ecommons.preferences.core.Preference;
import org.eclipse.statet.ecommons.ui.util.LayoutUtils;

import org.eclipse.statet.internal.ltk.ui.TemplatesMessages;
import org.eclipse.statet.ltk.ui.templates.config.ITemplateCategoryConfiguration;


public class NewDocTemplateGenerateWizardPage extends WizardPage {
	
	
	private final String categoryId;
	private final ITemplateCategoryConfiguration config;
	
	private TemplateSelectionComposite templateSelectComposite;
	
	
	public NewDocTemplateGenerateWizardPage(final ITemplateCategoryConfiguration config,
			final String categoryId) {
		super("CodeGen");
		this.config= config;
		this.categoryId= categoryId;
		
		setTitle(TemplatesMessages.NewDocWizardPage_title);
		setDescription(TemplatesMessages.NewDocWizardPage_description);
	}
	
	
	public ContextTypeRegistry getContextTypeRegistry() {
		return this.config.getContextTypeRegistry();
	}
	
	public Template getTemplate() {
		if (this.templateSelectComposite != null) {
			return this.templateSelectComposite.getSelectedTemplate();
		}
		else {
			return this.config.getTemplates().findTemplate(this.categoryId,
					getDefaultTemplateName() );
		}
	}
	
	private String getDefaultTemplateName() {
		final Preference<String> defaultPref= this.config.getDefaultPref();
		if (defaultPref == null) {
			return null;
		}
		return EPreferences.getInstancePrefs().getPreferenceValue(defaultPref);
	}
	
	protected List<Template> getAvailableTemplates() {
		final List<? extends TemplatePersistenceData> templateDatas= this.config.getTemplates().getTemplates(this.categoryId);
		final List<Template> templates= new ArrayList<>(templateDatas.size());
		for (final TemplatePersistenceData templateData : templateDatas) {
			final Template template= templateData.getTemplate();
			if (getCategoryId(template).equals(this.categoryId)) {
				templates.add(template);
			}
		}
		return templates;
	}
	
	protected String getCategoryId(final Template template) {
		// org.eclipse.statet.ltk.ui.templates.config.CodeTemplateConfigurationBlock.getCategoryId(Template)
		final String name= template.getName();
		final int idx= name.indexOf(':');
		return (idx > 0) ? name.substring(0, idx) : name;
	}
	
	@Override
	public void createControl(final Composite parent) {
		final Group group= new Group(parent, SWT.NONE);
		group.setLayout(LayoutUtils.newGroupGrid(1));
		group.setText(TemplatesMessages.NewDocWizardPage_Template_group);
		
		this.templateSelectComposite= new TemplateSelectionComposite(group);
		this.templateSelectComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		this.templateSelectComposite.setConfigurator(this.config.createViewerConfiguator(null, null,
				this.templateSelectComposite.getPreview().getTemplateVariableProcessor(),
				null ));
		this.templateSelectComposite.setInput(getAvailableTemplates(), true,
				getContextTypeRegistry() );
		this.templateSelectComposite.setSelection(getDefaultTemplateName());
		
		setControl(group);
	}
	
}
