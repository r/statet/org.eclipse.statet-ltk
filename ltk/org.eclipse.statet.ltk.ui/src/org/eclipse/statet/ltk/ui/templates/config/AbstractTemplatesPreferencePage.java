/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.templates.config;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.jface.text.templates.Template;
import org.eclipse.jface.text.templates.TemplateContextType;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.text.templates.TemplatePersistenceData;
import org.eclipse.ui.texteditor.templates.TemplatePreferencePage;

import org.eclipse.statet.ecommons.preferences.ui.SettingsUpdater;
import org.eclipse.statet.ecommons.templates.TemplateVariableProcessor;

import org.eclipse.statet.internal.ltk.ui.PreviewSourceViewer;
import org.eclipse.statet.ltk.ui.sourceediting.SourceEditorViewerConfigurator;
import org.eclipse.statet.ltk.ui.sourceediting.ViewerSourceEditorAdapter;
import org.eclipse.statet.ltk.ui.templates.EnhTemplateStore;


public abstract class AbstractTemplatesPreferencePage extends TemplatePreferencePage {
	
	
	private final SourceEditorViewerConfigurator viewerConfigurator;
	private final TemplateVariableProcessor templateProcessor;
	
	private final SourceEditorViewerConfigurator dialogViewerConfigurator;
	private final TemplateVariableProcessor dialogTemplateProcessor;
	
	
	public AbstractTemplatesPreferencePage() {
		this.templateProcessor= new TemplateVariableProcessor();
		this.viewerConfigurator= createSourceViewerConfigurator(this.templateProcessor);
		
		this.dialogTemplateProcessor= new TemplateVariableProcessor();
		this.dialogViewerConfigurator= createSourceViewerConfigurator(this.dialogTemplateProcessor);
	}
	
	public AbstractTemplatesPreferencePage(final IPreferenceStore preferenceStore,
			final EnhTemplateStore templateStore) {
		this();
		
		setPreferenceStore(preferenceStore);
		setTemplateStore(templateStore.getWorkingCopy());
		setContextTypeRegistry(templateStore.getContextTypeRegistry());
	}
	
	
	protected abstract SourceEditorViewerConfigurator createSourceViewerConfigurator(
			TemplateVariableProcessor templateProcessor);
	
	
	@Override
	public void setVisible(final boolean visible) {
		final String title= getTitle();
		super.setVisible(visible);
		if (title != null && !title.isEmpty()) {
			setTitle(title);
		}
	}
	
	@Override
	protected SourceViewer createViewer(final Composite parent) {
		final SourceViewer viewer= new PreviewSourceViewer(parent,
				this.viewerConfigurator.getSourceViewerConfiguration().getPreferences() );
		
		final ViewerSourceEditorAdapter adapter= new ViewerSourceEditorAdapter(viewer, null);
		this.viewerConfigurator.setTarget(adapter);
		// updater
		new SettingsUpdater(this.viewerConfigurator, viewer.getControl());
		
		final IDocument document= new Document();
		this.viewerConfigurator.getDocumentSetupParticipant().setup(document);
		viewer.setDocument(document);
		
		return viewer;
	}
	
	@Override
	protected void updateViewerInput() {
		super.updateViewerInput();
		
		final IStructuredSelection selection= (IStructuredSelection) getTableViewer().getSelection();
		
		if (selection.size() == 1) {
			final TemplatePersistenceData data= (TemplatePersistenceData) selection.getFirstElement();
			final Template template= data.getTemplate();
			final TemplateContextType contextType= getContextTypeRegistry().getContextType(template.getContextTypeId());
			this.templateProcessor.setContextType(contextType);
			final AbstractDocument document= (AbstractDocument) getViewer().getDocument();
			configureContext(document, contextType, this.viewerConfigurator);
		}
	}
	
	@Override
	protected Template editTemplate(final Template template, final boolean edit, final boolean isNameModifiable) {
		final org.eclipse.statet.ltk.ui.templates.config.EditTemplateDialog dialog= new org.eclipse.statet.ltk.ui.templates.config.EditTemplateDialog(
				getShell(), template, edit,
				org.eclipse.statet.ltk.ui.templates.config.EditTemplateDialog.EDITOR_TEMPLATE,
				this.dialogViewerConfigurator, this.dialogTemplateProcessor, getContextTypeRegistry(),
				TemplateConfigUI.PREF_QUALIFIER ) {
			
			@Override
			protected void configureForContext(final TemplateContextType contextType) {
				super.configureForContext(contextType);
				final SourceViewer sourceViewer= getSourceViewer();
				final AbstractDocument document= (AbstractDocument) sourceViewer.getDocument();
				AbstractTemplatesPreferencePage.this.configureContext(document, contextType, getSourceViewerConfigurator());
			}
		};
		if (dialog.open() == Window.OK) {
			return dialog.getTemplate();
		}
		return null;
	}
	
	protected abstract void configureContext(AbstractDocument document,
			TemplateContextType contextType, SourceEditorViewerConfigurator configurator);
	
	
	@Override
	protected boolean isShowFormatterSetting() {
		return false;
	}
	
	@Override
	protected String getFormatterPreferenceKey() {
		return null;
	}
	
}
