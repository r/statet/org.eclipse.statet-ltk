/*=============================================================================#
 # Copyright (c) 2000, 2022 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation in Platform
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.ui.sourceediting;

import org.eclipse.jface.text.source.AnnotationPainter;
import org.eclipse.jface.text.source.AnnotationPainter.ITextStyleStrategy;
import org.eclipse.jface.text.source.IAnnotationAccess;
import org.eclipse.jface.text.source.IOverviewRuler;
import org.eclipse.jface.text.source.ISharedTextColors;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.swt.graphics.Color;

import org.eclipse.statet.internal.ltk.ui.OverwriteTextStyleStrategy;


/**
 * Source viewer decoration support with additional overwrite annotation type.
 */
public class SourceViewerDecorationSupport extends org.eclipse.ui.texteditor.SourceViewerDecorationSupport {
	
	
	private static ITextStyleStrategy fgOverwriteStrategy= new OverwriteTextStyleStrategy();
	
	
	/**
	 * Creates a new decoration support for the given viewer.
	 *
	 * @param sourceViewer the source viewer
	 * @param overviewRuler the viewer's overview ruler
	 * @param annotationAccess the annotation access
	 * @param sharedTextColors the shared text color manager
	 */
	public SourceViewerDecorationSupport(final ISourceViewer sourceViewer, final IOverviewRuler overviewRuler, final IAnnotationAccess annotationAccess, final ISharedTextColors sharedTextColors) {
		super(sourceViewer, overviewRuler, annotationAccess, sharedTextColors);
	}
	
	@Override
	protected AnnotationPainter createAnnotationPainter() {
		final AnnotationPainter painter= super.createAnnotationPainter();
		
		painter.addTextStyleStrategy("org.eclipse.statet.Overwrite", fgOverwriteStrategy);
		painter.addAnnotationType("org.eclipse.statet.ecommons.text.editorAnnotations.ContentAssistOverwrite", "org.eclipse.statet.Overwrite");
		painter.setAnnotationTypeColor("org.eclipse.statet.ecommons.text.editorAnnotations.ContentAssistOverwrite",
				new Color(255, 0, 0) );
		
		return painter;
	}
	
}
