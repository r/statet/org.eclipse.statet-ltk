/*=============================================================================#
 # Copyright (c) 2000, 2022 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

// org.eclipse.jdt.internal.ui.preferences.JavaSourcePreviewerUpdater

package org.eclipse.statet.ecommons.text.ui;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Font;

import org.eclipse.statet.ecommons.ui.util.UIAccess;


/**
 * Handles editor font and properties changes for source viewers.
 * <p>
 * It disposes itself automatically.</p>
 */
public class TextViewerJFaceUpdater {
	
	
	protected final ISourceViewer fViewer;
	protected final IPreferenceStore fPreferenceStore;
	
	private IPropertyChangeListener fFontChangeListener;
	
	private final String fSymbolicFontName;
	
	
	/**
	 * Creates a source preview updater for the given viewer and preference store.
	 * 
	 * @param viewer the viewer
	 * @param preferenceStore the preference store
	 */
	public TextViewerJFaceUpdater(final ISourceViewer viewer, final IPreferenceStore preferenceStore) {
		this(viewer, preferenceStore, JFaceResources.TEXT_FONT);
	}
	
	public TextViewerJFaceUpdater(final ISourceViewer viewer, final IPreferenceStore preferenceStore,
			final String symbolicFontName) {
		assert (viewer != null);
		assert (preferenceStore != null);
		assert (symbolicFontName != null);
		
		this.fViewer= viewer;
		this.fPreferenceStore= preferenceStore;
		
		this.fSymbolicFontName= symbolicFontName;
		
		viewer.getTextWidget().addDisposeListener(new DisposeListener() {
			@Override
			public void widgetDisposed(final DisposeEvent e) {
				dispose();
			}
		});
		
		this.fFontChangeListener= new IPropertyChangeListener() {
			@Override
			public void propertyChange(final PropertyChangeEvent event) {
				if (TextViewerJFaceUpdater.this.fSymbolicFontName.equals(event.getProperty())) {
					updateFont();
				}
			}
		};
		JFaceResources.getFontRegistry().addListener(this.fFontChangeListener);
		
		updateFont();
	}
	
	
	protected void updateFont() {
		final Font font= JFaceResources.getFont(this.fSymbolicFontName);
		final StyledText styledText= this.fViewer.getTextWidget();
		if (UIAccess.isOkToUse(styledText) && font != null) {
			this.fViewer.getTextWidget().setFont(font);
		}
	}
	
	
	public final void dispose() {
		if (this.fFontChangeListener != null) {
			JFaceResources.getFontRegistry().removeListener(this.fFontChangeListener);
		}
		this.fFontChangeListener= null;
	}
	
}
