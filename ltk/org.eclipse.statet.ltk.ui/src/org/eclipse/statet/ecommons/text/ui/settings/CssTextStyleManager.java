/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ecommons.text.ui.settings;

import static org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants.TEXTSTYLE_BOLD_SUFFIX;
import static org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants.TEXTSTYLE_COLOR_SUFFIX;
import static org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants.TEXTSTYLE_ITALIC_SUFFIX;
import static org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants.TEXTSTYLE_STRIKETHROUGH_SUFFIX;
import static org.eclipse.statet.ecommons.text.ui.presentation.ITextPresentationConstants.TEXTSTYLE_UNDERLINE_SUFFIX;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.swt.graphics.RGB;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class CssTextStyleManager extends PreferenceStoreTextStyleManager<@Nullable String> {
	
	
	public static final String ROOT_STYLE_KEY= "ROOT"; //$NON-NLS-1$
	
	
	private static void appendCssColor(final StringBuilder sb, final RGB color) {
		sb.append('#');
		String s= Integer.toHexString(color.red);
		if (s.length() == 1) {
			sb.append('0');
		}
		sb.append(s);
		s= Integer.toHexString(color.green);
		if (s.length() == 1) {
			sb.append('0');
		}
		sb.append(s);
		s= Integer.toHexString(color.blue);
		if (s.length() == 1) {
			sb.append('0');
		}
		sb.append(s);
	}
	
	
	private final String defaultStyleKey;
	
	
	public CssTextStyleManager(final IPreferenceStore preferenceStore, final String stylesGroupId,
			final String defaultStyleKey) {
		super(preferenceStore, stylesGroupId);
		this.defaultStyleKey= defaultStyleKey;
	}
	
	
	@SuppressWarnings("null")
	public TextStyleToken<String> getRootStyleToken() {
		return getToken(ROOT_STYLE_KEY);
	}
	
	
	@Override
	protected @Nullable String createStyleData(String key) {
		if (key != ROOT_STYLE_KEY) {
			key= resolveUsedKey(key);
			if (key.equals(this.defaultStyleKey)) {
				return null;
			}
		}
		else {
			key= this.defaultStyleKey;
		}
		
		final StringBuilder sb= new StringBuilder(32);
		final RGB rgb= PreferenceConverter.getColor(this.preferenceStore, key + TEXTSTYLE_COLOR_SUFFIX);
		sb.append("color: "); //$NON-NLS-1$
		appendCssColor(sb, rgb);
		sb.append("; "); //$NON-NLS-1$
		if (this.preferenceStore.getBoolean(key + TEXTSTYLE_BOLD_SUFFIX)) {
			sb.append("font-weight: bold; "); //$NON-NLS-1$
		}
		if (this.preferenceStore.getBoolean(key + TEXTSTYLE_ITALIC_SUFFIX)) {
			sb.append("font-style: italic; "); //$NON-NLS-1$
		}
		final boolean strikethrough= this.preferenceStore.getBoolean(key + TEXTSTYLE_STRIKETHROUGH_SUFFIX);
		final boolean underline= this.preferenceStore.getBoolean(key + TEXTSTYLE_UNDERLINE_SUFFIX);
		if (strikethrough || underline) {
			sb.append("text-decoration:"); //$NON-NLS-1$
			if (strikethrough) {
				sb.append(" line-through"); //$NON-NLS-1$
			}
			if (underline) {
				sb.append(" underline"); //$NON-NLS-1$
			}
			sb.append("; "); //$NON-NLS-1$
		}
		
		return sb.substring(0, sb.length()-1);
	}
	
}
