/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ltk.ui;

import org.eclipse.osgi.util.NLS;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
@SuppressWarnings("null")
public class TemplatesMessages extends NLS {
	
	
	public static String Templates_Variable_AuthorName_description;
	public static String Templates_Variable_AuthorEmail_description;
	
	public static String Templates_Variable_EnclosingProject_description;
	public static String Templates_Variable_File_description;
	public static String Templates_Variable_SelectionBegin_description;
	public static String Templates_Variable_SelectionEnd_description;
	
	public static String Templates_Variable_ToDo_description;
	
	public static String Preview_label;
	
	public static String Config_DocTemplates_label;
	
	public static String Config_error_Read_message;
	public static String Config_error_Write_message;
	public static String Config_RestoreDefaults_title;
	public static String Config_RestoreDefaults_Completely_label;
	public static String Config_RestoreDefaults_Deleted_label;
	
	public static String Config_Import_title;
	public static String Config_Export_title;
	public static String Config_Export_filename;
	public static String Config_Export_error_title;
	public static String Config_Export_error_Hidden_message;
	public static String Config_Export_error_CanNotWrite_message;
	public static String Config_Export_Exists_title;
	public static String Config_Export_Exists_message;
	
	public static String NewDocWizardPage_title;
	public static String NewDocWizardPage_description;
	public static String NewDocWizardPage_Template_group;
	
	
	static {
		NLS.initializeMessages(TemplatesMessages.class.getName(), TemplatesMessages.class);
	}
	private TemplatesMessages() {}
	
}
