/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ltk.ui;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.content.IContentDescription;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;

import org.eclipse.statet.ltk.core.IExtContentTypeManager;
import org.eclipse.statet.ltk.core.Ltk;


/**
 * @see IExtContentTypeManager
 */
public class ActivatedContentTypeTester extends PropertyTester {
	
	
	public static final String MATCH_ACTIVATED_TYPE= "matchesActivatedContentType"; //$NON-NLS-1$
	public static final String MATCH_TYPE= "matchesContentType"; //$NON-NLS-1$
	
	
	@Override
	public boolean test(final Object receiver, final String property, final Object[] args, final Object expectedValue) {
		if (!(expectedValue instanceof String)) {
			return false;
		}
		IContentType contentType= null;
		
		// Search IFile
		IFile file= null;
		if (receiver instanceof IFile) {
			file= (IFile) receiver;
		}
		else if (receiver instanceof IAdaptable) {
			final IAdaptable adaptableReceiver= (IAdaptable) receiver;
			file= adaptableReceiver.getAdapter(IFile.class);
			if (file == null) {
				final IResource resource= adaptableReceiver.getAdapter(IResource.class);
				if (resource instanceof IFile) {
					file= (IFile) resource;
				}
			}
		}
		if (file == null) {
			IEditorInput editorInput;
			if (receiver instanceof IEditorInput) {
				editorInput= (IEditorInput) receiver;
			}
			else if (receiver instanceof IEditorPart) {
				editorInput= ((IEditorPart) receiver).getEditorInput();
			}
			else {
				editorInput= null;
			}
			if (editorInput != null) {
				file= editorInput.getAdapter(IFile.class);
			}
		}
		if (file != null) {
			// get content type by IFile
			try {
				final IContentDescription contentDescription= file.getContentDescription();
				if (contentDescription != null) {
					contentType= contentDescription.getContentType();
				}
			}
			catch (final CoreException e) {}
		}
		else {
			// get content type by SourceEditor
			if (receiver instanceof IAdaptable) {
				contentType= ((IAdaptable) receiver).getAdapter(IContentType.class);
			}
		}
		
		if (property.equals(MATCH_ACTIVATED_TYPE)) {
			final String expectedContentTypeId= (String) expectedValue;
			if (contentType != null) {
				return Ltk.getExtContentTypeManager().matchesActivatedContentType(
						contentType.getId(), expectedContentTypeId, true);
			}
			return false;
		}
		if (property.equals(MATCH_TYPE)) {
			final String expectedContentTypeId= (String) expectedValue;
			while (contentType != null) {
				if (expectedContentTypeId.equals(contentType.getId())) {
					return true;
				}
				contentType= contentType.getBaseType();
			}
			return false;
		}
		return false;
	}
	
}
