/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ltk.ui.sourceediting;

import org.eclipse.jface.text.source.IAnnotationAccessExtension;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.texteditor.AnnotationPreference;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.ui.LtkUI;


@NonNullByDefault
public class AnnotationPresentationConfig {
	
	private final int level;
	
	private final @Nullable String defaultImageId;
	private @Nullable Image defaultImage;
	
	private final @Nullable String awayImageId;
	private @Nullable Image awayImage;
	
	
	public AnnotationPresentationConfig(final @Nullable String referenceType, final int levelDiff,
			final @Nullable String defaultImageId, final @Nullable String awayImageId) {
		final AnnotationPreference preference= EditorsUI.getAnnotationPreferenceLookup().getAnnotationPreference(referenceType);
		
		if (levelDiff != Integer.MIN_VALUE) {
			this.level= ((preference != null) ?
							preference.getPresentationLayer() :
							IAnnotationAccessExtension.DEFAULT_LAYER ) +
					levelDiff;
		}
		else {
			this.level= 0;
		}
		
		this.defaultImageId= defaultImageId;
		this.awayImageId= awayImageId;
	}
	
	public final int getLevel() {
		return this.level;
	}
	
	public final @Nullable Image getImage() {
		Image image= this.defaultImage;
		if (image == null) {
			final String imageId= this.defaultImageId;
			if (imageId != null) {
				image= LtkUI.getUIResources().getImage(imageId);
				this.defaultImage= image;
			}
		}
		return image;
	}
	
	public final @Nullable Image getAwayImage() {
		Image image= this.awayImage;
		if (image == null) {
			final String imageId= this.awayImageId;
			if (imageId != null) {
				image= LtkUI.getUIResources().getImage(imageId);
				this.awayImage= image;
			}
		}
		return image;
	}
	
}