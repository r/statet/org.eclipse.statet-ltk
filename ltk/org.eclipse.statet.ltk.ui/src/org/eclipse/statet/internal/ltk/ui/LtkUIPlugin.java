/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ltk.ui;

import java.util.ArrayList;
import java.util.List;

import org.osgi.framework.BundleContext;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.statushandlers.StatusManager;

import org.eclipse.statet.jcommons.lang.Disposable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.util.ImageRegistryUtil;

import org.eclipse.statet.ltk.ui.LtkUI;
import org.eclipse.statet.ltk.ui.LtkUIResources;


@NonNullByDefault
public class LtkUIPlugin extends AbstractUIPlugin {
	
	
	private static LtkUIPlugin instance;
	
	/**
	 * Returns the shared plug-in instance
	 *
	 * @return the shared instance
	 */
	public static LtkUIPlugin getInstance() {
		return instance;
	}
	
	
	public static final void log(final IStatus status) {
		final Plugin plugin= getInstance();
		if (plugin != null) {
			plugin.getLog().log(status);
		}
	}
	
	
	private boolean started;
	
	private final List<Disposable> disposables= new ArrayList<>();
	
	private WorkbenchLabelProvider workbenchLabelProvider;
	
	
	public LtkUIPlugin() {
	}
	
	
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		instance= this;
		
		this.started= true;
	}
	
	@Override
	public void stop(final BundleContext context) throws Exception {
		try {
			synchronized (this) {
				this.started= false;
				
				if (this.workbenchLabelProvider != null) {
					try {
						if (PlatformUI.isWorkbenchRunning() &&
								!PlatformUI.getWorkbench().isClosing()) {
							this.workbenchLabelProvider.dispose();
						}
					}
					catch (final Exception e) {
						StatusManager.getManager().handle(new Status(IStatus.ERROR, LtkUI.BUNDLE_ID, -1,
								"An error occurred when disposing the shared WorkbenchLabelProvider.", e));
					}
					this.workbenchLabelProvider= null;
				}
			}
			
			for (final Disposable listener : this.disposables) {
				try {
					listener.dispose();
				}
				catch (final Throwable e) {
					log(new Status(IStatus.ERROR, LtkUI.BUNDLE_ID,
							"Error occured when dispose module",
							e ));
				}
			}
			this.disposables.clear();
		}
		finally {
			instance= null;
			super.stop(context);
		}
	}
	
	
	public void addStoppingListener(final Disposable listener) {
		if (listener == null) {
			throw new NullPointerException();
		}
		synchronized (this) {
			if (!this.started) {
				throw new IllegalStateException("Plug-in is not started.");
			}
			this.disposables.add(listener);
		}
	}
	
	@Override
	protected void initializeImageRegistry(final ImageRegistry reg) {
		if (!this.started) {
			throw new IllegalStateException("Plug-in is not started.");
		}
		final ImageRegistryUtil util= new ImageRegistryUtil(this);
		
		util.register(LtkUIResources.OBJ_ERROR_IMAGE_ID, ImageRegistryUtil.T_OBJ, "error.png"); //$NON-NLS-1$
		util.register(LtkUIResources.OBJ_ERROR_AWAY_IMAGE_ID, ImageRegistryUtil.T_OBJ, "error-away.png"); //$NON-NLS-1$
		util.register(LtkUIResources.OBJ_WARNING_IMAGE_ID, ImageRegistryUtil.T_OBJ, "warning.png"); //$NON-NLS-1$
		util.register(LtkUIResources.OBJ_WARNING_AWAY_IMAGE_ID, ImageRegistryUtil.T_OBJ, "warning-away.png"); //$NON-NLS-1$
		util.register(LtkUIResources.OBJ_INFO_IMAGE_ID, ImageRegistryUtil.T_OBJ, "info.png"); //$NON-NLS-1$
		util.register(LtkUIResources.OBJ_INFO_AWAY_IMAGE_ID, ImageRegistryUtil.T_OBJ, "info-away.png"); //$NON-NLS-1$
		
		util.register(LtkUIResources.OBJ_TEXT_TEMPLATE_IMAGE_ID, ".png"); //$NON-NLS-1$
		util.register(LtkUIResources.OBJ_TEXT_AT_TAG_IMAGE_ID, ".png"); //$NON-NLS-1$
		util.register(LtkUIResources.OBJ_TEXT_LINKEDRENAME_IMAGE_ID, ".png"); //$NON-NLS-1$
	}
	
	
	/**
	 * Access to resource decoration.
	 */
	public synchronized WorkbenchLabelProvider getWorkbenchLabelProvider() {
		if (this.workbenchLabelProvider == null) {
			if (!this.started) {
				throw new IllegalStateException("Plug-in is not started.");
			}
			this.workbenchLabelProvider= new WorkbenchLabelProvider();
		}
		return this.workbenchLabelProvider;
	}
	
}
