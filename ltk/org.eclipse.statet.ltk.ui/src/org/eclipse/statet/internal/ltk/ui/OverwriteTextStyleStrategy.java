/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ltk.ui;

import java.util.Set;

import org.eclipse.jface.text.source.AnnotationPainter.ITextStyleStrategy;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.graphics.Color;

import org.eclipse.statet.jcommons.lang.Disposable;

import org.eclipse.statet.ecommons.preferences.SettingsChangeNotifier;


public class OverwriteTextStyleStrategy implements ITextStyleStrategy, Disposable, SettingsChangeNotifier.ChangeListener {
	
	
	private Color color;
	
	
	public OverwriteTextStyleStrategy() {
//		PreferencesUtil.getSettingsChangeNotifier().addChangeListener(this);
	}
	
	
	@Override
	public void applyTextStyle(final StyleRange styleRange, final Color annotationColor) {
		Color color= this.color;
		if (this.color == null) {
			this.color= color= initColor(annotationColor);
		}
		styleRange.strikeout= true;
		styleRange.strikeoutColor= color;
		styleRange.foreground= color;
	}
	
	
	public Color initColor(final Color fallback) {
//		final RGB rgb= ;
//		if (rgb != null) {
//			return ECommonsUI.getColors().getColor(rgb);
//		}
		return fallback;
	}
	
	@Override
	public void settingsChanged(final Set<String> groupIds) {
//		if (groupIds.contains(GROUP_ID)) {
//			this.color= null;
//		}
	}
	
	@Override
	public void dispose() {
//		PreferencesUtil.getSettingsChangeNotifier().removeChangeListener(this);
	}
	
}
