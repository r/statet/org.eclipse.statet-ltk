/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ltk.ui;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.commands.IParameterValues;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;

import org.eclipse.statet.ecommons.ui.util.MessageUtils;


/**
 * Map of parameters for the specific content assist command.
 */
public class SpecificContentAssistParameterValues implements IParameterValues {
	
	
	private Map<String, String> parameterValues;
	
	
	public SpecificContentAssistParameterValues() {
	}
	
	
	@Override
	public Map<String, String> getParameterValues() {
		Map<String, String> map= this.parameterValues;
		if (map == null) {
			map= new HashMap<>();
			
			final IExtensionRegistry extensionRegistry= Platform.getExtensionRegistry();
			final IConfigurationElement[] contributions= extensionRegistry.getConfigurationElementsFor(AdvancedExtensionsInternal.CONTENTASSIST_EXTENSIONPOINT_ID);
			for (final IConfigurationElement config : contributions) {
				if (config.getName().equals(AdvancedExtensionsInternal.CONFIG_CATEGORY_ELEMENT_NAME)) {
					try {
						final String id= AdvancedExtensionsInternal.getCheckedString(config, AdvancedExtensionsInternal.CONFIG_ID_ATTRIBUTE_NAME);
						final String name= AdvancedExtensionsInternal.getCheckedString(config, AdvancedExtensionsInternal.CONFIG_NAME_ATTRIBUTE_NAME);
						map.put(MessageUtils.removeMnemonics(name), id);
					}
					catch (final CoreException e) {
					}
				}
			}
			this.parameterValues= map;
		}
		return map;
	}
	
}
