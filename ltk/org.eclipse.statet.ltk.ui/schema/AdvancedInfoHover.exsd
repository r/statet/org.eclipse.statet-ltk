<?xml version='1.0' encoding='UTF-8'?>
<schema targetNamespace="org.eclipse.statet.ltk" xmlns="http://www.w3.org/2001/XMLSchema">
<annotation>
      <appInfo>
         <meta.schema id="AdvancedInfoHover"
               plugin="org.eclipse.statet.ltk.ui"
               name="Point to register information hovers"/>
      </appInfo>
      <documentation>
         This extension-point allows to provide information hovers to content types.
      </documentation>
   </annotation>

   <element name="extension">
      <annotation>
         <appInfo>
            <meta.element />
         </appInfo>
      </annotation>
      <complexType>
         <choice minOccurs="0" maxOccurs="unbounded">
            <element ref="hover"/>
         </choice>
         <attribute name="point" type="string" use="required">
            <annotation>
               <documentation>
                  
               </documentation>
            </annotation>
         </attribute>
         <attribute name="id" type="string">
            <annotation>
               <documentation>
                  The id of the extension.
               </documentation>
            </annotation>
         </attribute>
         <attribute name="name" type="string">
            <annotation>
               <documentation>
                  
               </documentation>
               <appInfo>
                  <meta.attribute translatable="true"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <element name="hover">
      <annotation>
         <documentation>
            Defines a hover type.
         </documentation>
      </annotation>
      <complexType>
         <attribute name="id" type="string" use="required">
            <annotation>
               <documentation>
                  The id of this hover type.
               </documentation>
               <appInfo>
                  <meta.attribute kind="identifier"/>
               </appInfo>
            </annotation>
         </attribute>
         <attribute name="name" type="string" use="required">
            <annotation>
               <documentation>
                  The name of this hover type.
               </documentation>
               <appInfo>
                  <meta.attribute translatable="true"/>
               </appInfo>
            </annotation>
         </attribute>
         <attribute name="contentTypeId" type="string" use="required">
            <annotation>
               <documentation>
                  The id of the content type this hover will be loaded for.
               </documentation>
               <appInfo>
                  <meta.attribute kind="identifier" basedOn="org.eclipse.core.contenttype.contentTypes/content-type/@id"/>
               </appInfo>
            </annotation>
         </attribute>
         <attribute name="class" type="string" use="required">
            <annotation>
               <documentation>
                  The implementation of the hover.
               </documentation>
               <appInfo>
                  <meta.attribute kind="java" basedOn=":org.eclipse.statet.ltk.ui.sourceediting.IInfoHover"/>
               </appInfo>
            </annotation>
         </attribute>
      </complexType>
   </element>

   <annotation>
      <appInfo>
         <meta.section type="since"/>
      </appInfo>
      <documentation>
         0.9
      </documentation>
   </annotation>

   <annotation>
      <appInfo>
         <meta.section type="copyright"/>
      </appInfo>
      <documentation>
         Copyright (c) 2010, 2022 Stephan Wahlbrink and others.

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
which is available at https://www.apache.org/licenses/LICENSE-2.0.

SPDX-License-Identifier: EPL-2.0 OR Apache-2.0

Contributors:
    Stephan Wahlbrink &lt;sw@wahlbrink.eu&gt; - initial API and implementation
      </documentation>
   </annotation>

</schema>
