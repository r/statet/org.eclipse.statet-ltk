/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.core.source;

import org.eclipse.jface.text.IDocumentExtension4;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * The version of a source model like an AST.
 * 
 * Beside the {@link #getContentStamp() source content stamp} it can e.g. consider the language
 * configuration used when parsing the source.
 */
@NonNullByDefault
public interface SourceModelStamp {
	
	
	/**
	 * Returns the source content stamp.
	 * 
	 * @see SourceContent#getStamp()
	 * @see IDocumentExtension4#getModificationStamp()
	 * 
	 * @return the stamp
	 */
	long getContentStamp();
	
	<T> @Nullable T getConfig(Class<T> type);
	
}
