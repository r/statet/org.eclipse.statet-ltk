/*=============================================================================#
 # Copyright (c) 2010, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.core.input;

import org.eclipse.jface.text.AbstractDocument;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


@NonNullByDefault
public class BasicSourceFragment implements SourceFragment {
	
	
	private final String id;
	
	private final String name;
	private final String fullName;
	
	private final AbstractDocument document;
	
	
	public BasicSourceFragment(final String id, final String name, final String fullName,
			final AbstractDocument document) {
		this.id= id;
		this.name = name;
		this.fullName = fullName;
		this.document = document;
	}
	
	
	@Override
	public String getId() {
		return this.id;
	}
	
	@Override
	public String getName() {
		return this.name;
	}
	
	@Override
	public String getFullName() {
		return this.fullName;
	}
	
	@Override
	public AbstractDocument getDocument() {
		return this.document;
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public <T> @Nullable T getAdapter(final Class<T> adapterType) {
		return null;
	}
	
	
	@Override
	public int hashCode() {
		return this.id.hashCode();
	}
	
	protected Class<?> getFragmentType() {
		return SourceFragment.class;
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof BasicSourceFragment) {
			final BasicSourceFragment other= (BasicSourceFragment) obj;
			return (getFragmentType() == other.getFragmentType()
					&& this.id.equals(other.id)
					&& this.fullName.equals(other.fullName)
					&& this.document.equals(other.document) );
		}
		return false;
	}
	
	@Override
	public String toString() {
		return this.fullName;
	}
	
}
