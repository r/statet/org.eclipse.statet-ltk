/*=============================================================================#
 # Copyright (c) 2015, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.core.source;

import java.util.Objects;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;
import org.eclipse.statet.jcommons.model.core.AttachmentsElement;
import org.eclipse.statet.jcommons.text.core.BasicTextRegion;


/**
 * Detail for the status code of an AST node.
 */
@NonNullByDefault
public class StatusDetail extends BasicTextRegion {
	
	
	public static final @Nullable StatusDetail getStatusDetail(final AttachmentsElement element) {
		for (final Object attachment : element.getAttachments()) {
			if (attachment instanceof StatusDetail) {
				return (StatusDetail)attachment;
			}
		}
		return null;
	}
	
	
	private final @Nullable String text;
	
	
	public StatusDetail(final int startOffset, final int endOffset, final @Nullable String text) {
		super(startOffset, endOffset);
		this.text= text;
	}
	
	
	public @Nullable String getText() {
		return this.text;
	}
	
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof StatusDetail) {
			final StatusDetail other= (StatusDetail)obj;
			return (equalsByOffsets(other)
					&& Objects.equals(this.text, other.text) );
		}
		return false;
	}
	
}
