/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.core;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.internal.ltk.core.LtkCorePlugin;


@NonNullByDefault
public class Ltk {
	
	
	public static final WorkingContext PERSISTENCE_CONTEXT= new WorkingContext("persistence.default"); //$NON-NLS-1$
	
	public static final WorkingContext EDITOR_CONTEXT= new WorkingContext("editor.default"); //$NON-NLS-1$
	
	
	public static IExtContentTypeManager getExtContentTypeManager() {
		return LtkCorePlugin.getSafe().getContentTypeServices();
	}
	
}
