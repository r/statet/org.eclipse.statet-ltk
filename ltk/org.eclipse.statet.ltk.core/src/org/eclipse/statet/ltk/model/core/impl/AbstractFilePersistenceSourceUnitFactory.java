/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.impl;

import java.net.URI;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.core.Ltk;
import org.eclipse.statet.ltk.model.core.SourceUnitFactory;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;


/**
 * Abstract factory for {@link Ltk#PERSISTENCE_CONTEXT}.
 */
@NonNullByDefault
public abstract class AbstractFilePersistenceSourceUnitFactory implements SourceUnitFactory {
	
	
	private static final String IFILE_PREFIX= "platform:/resource"; //$NON-NLS-1$
	
	
	public static String createResourceId(final IResource file) {
		final IPath path= file.getFullPath();
		return IFILE_PREFIX + path.toPortableString(); // eclipse-platform-resource
	}
	
	public static String createResourceId(URI uri) {
		uri= uri.normalize();
		if (uri.getScheme() == null) {
			return "xxx:"+uri.toString(); //$NON-NLS-1$
		}
		else {
			return uri.toString();
		}
	}
	
	
	public AbstractFilePersistenceSourceUnitFactory() {
	}
	
	
	@Override
	public @Nullable String createId(final Object from) {
		if (from instanceof IFile) {
			return createResourceId((IFile)from);
		}
		if (from instanceof String) {
			final String s= (String)from;
			if (s.startsWith(IFILE_PREFIX)) {
				return s;
			}
		}
		return null;
	}
	
	@Override
	public @Nullable SourceUnit createSourceUnit(final String id, final Object from) {
		final IFile ifile;
		if (from instanceof IFile) {
			ifile= (IFile)from;
		}
		else {
			final IPath path= Path.fromPortableString(id.substring(IFILE_PREFIX.length()));
			ifile= ResourcesPlugin.getWorkspace().getRoot().getFile(path);
		}
		return createSourceUnit(id, ifile);
	}
	
	
	protected abstract SourceUnit createSourceUnit(final String id, final IFile file);
	
}
