/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.impl;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.AbstractDocument;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;


/**
 * Interface to access documents. 
 * Usually only used inside implementations of {@link SourceUnit}.
 * <p>
 * For the progress monitors of the methods the SubMonitor pattern is applied.</p>
 */
@NonNullByDefault
public interface WorkingBuffer {
	
	
	long getContentStamp(final @Nullable IProgressMonitor monitor);
	
	/**
	 * @return the document or <code>null</code>
	 */
	@Nullable AbstractDocument getDocument();
	
	/**
	 * @return the document
	 */
	AbstractDocument getDocument(final @Nullable IProgressMonitor monitor);
	SourceContent getContent(final IProgressMonitor monitor);
	boolean checkState(final boolean validate, final IProgressMonitor monitor);
	boolean isSynchronized();
	void saveDocument(final IProgressMonitor monitor);
	void releaseDocument(final IProgressMonitor monitor);
	
}
