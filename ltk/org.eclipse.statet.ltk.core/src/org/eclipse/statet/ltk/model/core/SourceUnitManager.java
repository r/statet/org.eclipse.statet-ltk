/*=============================================================================#
 # Copyright (c) 2009, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.content.IContentType;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.core.WorkingContext;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;


/**
 * Manages shared instances of source units.
 */
@NonNullByDefault
public interface SourceUnitManager {
	
	/**
	 * Returns the source unit for the given object in the given LTK model and working context.
	 * 
	 * The supported object types depends on the model and working context. Typically for
	 * the workspace context it is IFile, for the editor context an existing source unit
	 * (a working copy is created) or IFileStore (URI).
	 * The returned source unit is already connected. If it is a working copy of an existing source
	 * unit (if <code>from</code> was instance of SourceUnit), the parent is disconnected.
	 * 
	 * @param modelTypeId the model type id
	 * @param context the working context
	 * @param from the object to get the source unit for
	 * @param create whether a new source unit object should be created, if it does not yet exists
	 * @param monitor
	 * @return the source unit or <code>null</code>
	 */
	@Nullable SourceUnit getSourceUnit(final String modelTypeId, final WorkingContext context, final Object from,
			final boolean create, final IProgressMonitor monitor);
	
	@Nullable SourceUnit getSourceUnit(final WorkingContext context, final Object from,
			final @Nullable IContentType contentType,
			final boolean create, final IProgressMonitor monitor);
	
	List<SourceUnit> getOpenSourceUnits(final List<String> modelTypeIds,
			final @Nullable WorkingContext context);
	List<SourceUnit> getOpenSourceUnits(final String modelTypeId,
			final @Nullable WorkingContext context);
	List<SourceUnit> getOpenSourceUnits(final List<String> modelTypeIds,
			final @Nullable WorkingContext context, final Object from);
	
}
