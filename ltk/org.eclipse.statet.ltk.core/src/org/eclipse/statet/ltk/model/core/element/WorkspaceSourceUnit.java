/*=============================================================================#
 # Copyright (c) 2011, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.element;

import org.eclipse.core.resources.IResource;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.text.IMarkerPositionResolver;


/**
 * Source unit of a workspace resource
 */
@NonNullByDefault
public interface WorkspaceSourceUnit extends SourceUnit {
	
	
	/**
	 * The file resource of the source unit in the workspace.
	 * 
	 * @return the resource
	 */
	@Override
	IResource getResource();
	
	/**
	 * Returns a resolver for markers in the resource providing the position in the current
	 * document content of the source unit.
	 * 
	 * @return the resolver if required
	 */
	@Nullable IMarkerPositionResolver getMarkerPositionResolver();
	
}
