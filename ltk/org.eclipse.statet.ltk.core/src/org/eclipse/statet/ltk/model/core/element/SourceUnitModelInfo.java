/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.element;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.model.core.AttachmentsElement;

import org.eclipse.statet.ltk.ast.core.AstInfo;
import org.eclipse.statet.ltk.core.source.SourceModelStamp;


/**
 * Container for model information of an source unit
 */
@NonNullByDefault
public interface SourceUnitModelInfo extends AttachmentsElement {
	
	
	/**
	 * Returns the stamp of the model.
	 * 
	 * @return the stamp
	 */
	SourceModelStamp getStamp();
	
	/**
	 * The AST used to create this info.
	 * 
	 * @return the AST information
	 */
	AstInfo getAst();
	
	/**
	 * The source unit as source structure model element.
	 * 
	 * @return the element
	 */
	SourceStructElement<?, ?> getSourceElement();
	
}
