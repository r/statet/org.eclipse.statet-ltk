/*=============================================================================#
 # Copyright (c) 2007, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core;

import org.eclipse.core.runtime.IProgressMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.core.WorkingContext;
import org.eclipse.statet.ltk.model.core.build.SourceUnitModelContainer;
import org.eclipse.statet.ltk.model.core.element.SourceUnit;


/**
 * 
 */
@NonNullByDefault
public interface ModelManager {
	
	int NONE=                                              0x00_00_00_00;
	
	int AST=                                                0x00_00_00_01;
	int MODEL_FILE=                                         0x00_00_00_02;
	int MODEL_DEPENDENCIES=                                 0x00_00_00_03;
	
	int REFRESH=                                            0x01_00_00_00;
	int RECONCILE=                                          0x02_00_00_00;
	
	
	/**
	 * Refreshes the model info of all loaded source units in given context.
	 */
	void refresh(final WorkingContext context);
	
	void addElementChangedListener(final ElementChangedListener listener, final WorkingContext context);
	void removeElementChangedListener(final ElementChangedListener listener, final WorkingContext context);
	
	void registerDependentUnit(final SourceUnit su);
	void deregisterDependentUnit(final SourceUnit su);
	
	void reconcile(final SourceUnitModelContainer<?, ?> adapter, final int level,
			final IProgressMonitor monitor);
	
}
