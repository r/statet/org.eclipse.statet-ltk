/*=============================================================================#
 # Copyright (c) 2012, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.model.core.element;

import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;


/**
 * Set of INameAccess in a (source) unit.
 * 
 * @param <TNameAccess> type of name access
 * 
 * @see org.eclipse.statet.ltk.model.core.impl.BasicNameAccessSet
 */
@NonNullByDefault
public interface NameAccessSet<TNameAccess extends NameAccess<?, TNameAccess>> {
	
	
	ImList<String> getNames();
	
	@Nullable ImList<TNameAccess> getAllInUnit(String label);
	
}
