/*=============================================================================#
 # Copyright (c) 2008, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.issues.core;

import java.util.List;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.status.StatusException;

import org.eclipse.statet.ltk.model.core.build.SourceUnitModelContainer;


/**
 * Accept problems by a problem checker.
 * 
 * {@link SourceUnitModelContainer#createIssueRequestor()}
 */
@NonNullByDefault
public interface IssueRequestor {
	
	
	boolean isInterestedInProblems(String categoryId);
	
	/**
	 * Notification of a discovered problem.
	 * 
	 * @param problem the problem.
	 */
	void acceptProblems(Problem problem);
	
	/**
	 * Notification of a list of problems.
	 * 
	 * @param type the category of the problems.
	 * @param problems the problems.
	 */
	void acceptProblems(String categoryId, List<Problem> problems);
	
	
	boolean isInterestedInTasks();
	
	/**
	 * Notification of a discovered task.
	 * 
	 * @param task the task.
	 */
	void acceptTask(Task task);
	
	
	void finish() throws StatusException;
	
}
