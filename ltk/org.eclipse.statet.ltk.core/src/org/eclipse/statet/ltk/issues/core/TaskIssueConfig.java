/*=============================================================================#
 # Copyright (c) 2021, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.issues.core;

import java.util.Objects;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.Immutable;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.preferences.core.PreferenceAccess;
import org.eclipse.statet.ecommons.preferences.core.PreferenceObjectDef;
import org.eclipse.statet.ecommons.preferences.core.PreferenceUtils;


@NonNullByDefault
public class TaskIssueConfig implements Immutable {
	
	
	private final ImList<TaskTag> taskTags;
	
	
	public TaskIssueConfig(final ImList<TaskTag> taskTags) {
		this.taskTags= taskTags;
	}
	
	public TaskIssueConfig(final PreferenceAccess prefs) {
		this.taskTags= Issues.loadTaskTags(prefs,
				Issues.TASK_TAG_KEYWORD_PREF, Issues.TASK_TAG_PRIORITY_PREF );
	}
	
	
	public ImList<TaskTag> getTaskTags() {
		return this.taskTags;
	}
	
	public @Nullable TaskTag getDefaultTaskTag() {
		return (!this.taskTags.isEmpty()) ? this.taskTags.get(0) : null;
	}
	
	
	@Override
	public int hashCode() {
		return this.taskTags.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof TaskIssueConfig) {
			final var other= (TaskIssueConfig)obj;
			return (this.taskTags.equals(other.getTaskTags())
					&& Objects.equals(getDefaultTaskTag(), other.getDefaultTaskTag()) );
		}
		return super.equals(obj);
	}
	
	
	
	private static final PreferenceObjectDef<TaskIssueConfig> CONFIG_PREF_FACTORY=
			PreferenceObjectDef.createFactory(TaskIssueConfig.class, ImCollections.newSet(
							Issues.TASK_TAG_QUALIFIER ),
					TaskIssueConfig::new );
	
	public static TaskIssueConfig getConfig(final PreferenceAccess prefs) {
		return PreferenceUtils.getPreferenceObject(prefs, CONFIG_PREF_FACTORY);
	}
	
}
