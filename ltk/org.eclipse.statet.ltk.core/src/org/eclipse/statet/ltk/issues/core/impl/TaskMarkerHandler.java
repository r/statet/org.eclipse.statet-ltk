/*=============================================================================#
 # Copyright (c) 2005, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.issues.core.impl;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.BadLocationException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.core.source.SourceContent;
import org.eclipse.statet.ltk.issues.core.IssueMarkers;
import org.eclipse.statet.ltk.issues.core.TaskPriority;
import org.eclipse.statet.ltk.issues.core.TaskTag;


@NonNullByDefault
public class TaskMarkerHandler {
	
	
	private final String markerType;
	
	private @Nullable Pattern taskTagPattern;
	private final Map<String, TaskPriority> taskTagMap= new HashMap<>();
	
	private SourceContent sourceContent= nonNullLateInit();
	private IResource resource= nonNullLateInit();
	private @Nullable Matcher taskTagMatcher;
	
	
	public TaskMarkerHandler(final String markerType) {
		this.markerType= markerType;
	}
	
	
	protected void initTaskPattern(final List<TaskTag> taskTags) {
		this.taskTagPattern= null;
		this.taskTagMap.clear();
		
		if (taskTags.isEmpty()) {
			return;
		}
		
		final String separatorRegex= "[^\\p{L}\\p{N}]"; //$NON-NLS-1$
		final StringBuilder regex= new StringBuilder(separatorRegex);
		regex.append('(');
		for (final TaskTag taskTag : taskTags) {
			regex.append(Pattern.quote(taskTag.getKeyword()));
			regex.append('|');
			this.taskTagMap.put(taskTag.getKeyword(), taskTag.getPriority());
		}
		regex.setCharAt(regex.length() - 1, ')');
		regex.append("(?:\\z|").append(separatorRegex).append(")"); //$NON-NLS-1$ //$NON-NLS-2$
 		this.taskTagPattern= Pattern.compile(regex.toString());
	}
	
	
	public void setup(final SourceContent sourceContent, final IResource resource) {
		this.sourceContent= nonNullAssert(sourceContent);
		this.resource= nonNullAssert(resource);
		
		final Pattern taskTagPattern= this.taskTagPattern;
		this.taskTagMatcher= (taskTagPattern != null && sourceContent.getStartOffset() == 0) ?
				taskTagPattern.matcher(sourceContent.getString()) :
				null;
	}
	
	public void addTaskMarker(final String message, final int offset, final int lineNumber,
			final String match) throws CoreException {
		final TaskPriority prio= this.taskTagMap.get(match);
		if (prio == null) {
			return;
		}
		
		final var attributes= new HashMap<String, Object>();
		attributes.put(IMarker.MESSAGE, message);
		attributes.put(IMarker.PRIORITY, IssueMarkers.toMarkerPriority(prio));
		attributes.put(IMarker.LINE_NUMBER, (lineNumber > 0) ? lineNumber : 1);
		if (offset != -1) {
			attributes.put(IMarker.CHAR_START, offset);
			attributes.put(IMarker.CHAR_END, offset + message.length());
		}
		attributes.put(IMarker.USER_EDITABLE, false);
		
		this.resource.createMarker(this.markerType, attributes);
	}
	
	public void removeTaskMarkers() throws CoreException {
		this.resource.deleteMarkers(this.markerType, false, IResource.DEPTH_INFINITE);
	}
	
	public void checkForTasks(final int startOffset, int endOffset)
			throws CoreException, BadLocationException {
		final Matcher matcher= this.taskTagMatcher;
		if (matcher == null) {
			return;
		}
		if (matcher.region(startOffset, endOffset).find()) {
			final int tagStartOffset= matcher.start(1);
			final int tagEndOffset= matcher.end(1);
			while (endOffset > tagEndOffset) {
				if (this.sourceContent.getChar(endOffset - 1) <= ' ') {
					endOffset--;
					continue;
				}
				else {
					break;
				}
			}
			final String text= this.sourceContent.getString(tagStartOffset, endOffset);
			addTaskMarker(text, tagStartOffset,
					this.sourceContent.getStringLines().getLineOfOffset(tagStartOffset) + 1,
					nonNullAssert(matcher.group(1)) );
		}
	}
	
}
