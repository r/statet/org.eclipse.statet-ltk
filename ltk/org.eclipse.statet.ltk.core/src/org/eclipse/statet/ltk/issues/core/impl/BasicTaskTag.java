/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.issues.core.impl;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.issues.core.TaskPriority;
import org.eclipse.statet.ltk.issues.core.TaskTag;


@NonNullByDefault
public class BasicTaskTag implements TaskTag {
	
	
	private final String keyword;
	
	private final TaskPriority priority;
	
	
	public BasicTaskTag(final String tag, final TaskPriority priority) {
		this.keyword= tag;
		this.priority= priority;
	}
	
	
	@Override
	public String getKeyword() {
		return this.keyword;
	}
	
	@Override
	public TaskPriority getPriority() {
		return this.priority;
	}
	
	
	@Override
	public int hashCode() {
		return this.keyword.hashCode();
	}
	
	@Override
	public boolean equals(@Nullable final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof TaskTag) {
			final var other= (TaskTag)obj;
			return (this.keyword.equals(other.getKeyword())
					&& this.priority == other.getPriority() );
		}
		return false;
	}
	
	@Override
	public String toString() {
		return this.keyword;
	}
	
}
