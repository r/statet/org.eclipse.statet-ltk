/*=============================================================================#
 # Copyright (c) 2014, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.project.core.builder;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullLateInit;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.SubMonitor;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ltk.model.core.element.WorkspaceSourceUnit;
import org.eclipse.statet.ltk.project.core.LtkProject;


@NonNullByDefault
public class ProjectBuildParticipant<TProject extends LtkProject,
		TSourceUnit extends WorkspaceSourceUnit> {
	
	
	TProject ltkProject= nonNullLateInit();
	
	int buildType;
	
	boolean enabled;
	
	
	public ProjectBuildParticipant() {
	}
	
	
	public final TProject getLtkProject() {
		return this.ltkProject;
	}
	
	public final int getBuildType() {
		return this.buildType;
	}
	
	public void init() {
	}
	
	protected final void setEnabled(final boolean enabled) {
		this.enabled= enabled;
	}
	
	public final boolean isEnabled() {
		return this.enabled;
	}
	
	
	/**
	 * @param sourceUnit the added/changed source unit
	 * @param monitor SubMonitor-recommended
	 */
	public void handleSourceUnitUpdated(final TSourceUnit sourceUnit,
			final SubMonitor m) throws CoreException {
		// update index
	}
	
	/**
	 * @param file the removed resource
	 * @param monitor SubMonitor-recommended
	 */
	public void handleSourceUnitRemoved(final IFile file,
			final SubMonitor m) throws CoreException {
		// remove from index
	}
	
	public void finish(final SubMonitor m) throws CoreException {
	}
	
}
