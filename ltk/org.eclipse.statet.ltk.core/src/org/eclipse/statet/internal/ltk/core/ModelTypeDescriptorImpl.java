/*=============================================================================#
 # Copyright (c) 2013, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ltk.core;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.statet.jcommons.collections.ImCollection;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.model.core.ModelTypeDescriptor;


@NonNullByDefault
public class ModelTypeDescriptorImpl implements ModelTypeDescriptor {
	
	
	private final String id;
	
	List<String> secondaryTypeIds= new ArrayList<>(1);
	ImList<String> checkedSecondaryTypeIds;
	
	
	public ModelTypeDescriptorImpl(final String id) {
		this.id= id;
	}
	
	
	@Override
	public String getId() {
		return this.id;
	}
	
	@Override
	public ImCollection<String> getSecondaryTypeIds() {
		return this.checkedSecondaryTypeIds;
	}
	
	
	@Override
	public int hashCode() {
		return this.id.hashCode();
	}
	
	@Override
	public boolean equals(final @Nullable Object obj) {
		return (obj instanceof ModelTypeDescriptorImpl
				&& this.id == ((ModelTypeDescriptorImpl)obj).id);
	}
	
	@Override
	public String toString() {
		return this.id;
	}
	
}
