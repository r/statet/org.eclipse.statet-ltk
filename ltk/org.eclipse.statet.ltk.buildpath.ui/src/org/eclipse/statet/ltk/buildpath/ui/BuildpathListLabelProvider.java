/*=============================================================================#
 # Copyright (c) 2010, 2022 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.buildpath.ui;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.util.List;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ecommons.ui.SharedUIResources;
import org.eclipse.statet.ecommons.ui.util.MessageUtils;

import org.eclipse.statet.internal.ltk.buildpath.ui.Messages;
import org.eclipse.statet.ltk.buildpath.core.BuildpathAttribute;
import org.eclipse.statet.ltk.buildpath.core.BuildpathElement;
import org.eclipse.statet.ltk.buildpath.core.BuildpathInitializer;
import org.eclipse.statet.ltk.ui.util.DecoratedElementImageDescriptor;


@NonNullByDefault
public class BuildpathListLabelProvider extends LabelProvider {
	
	
	private final BuildpathsUIResources buildpathsResources;
	private final SharedUIResources sharedResources;
	private final ISharedImages workbenchImages;
	
	
	public BuildpathListLabelProvider() {
		this.buildpathsResources= BuildpathsUI.getUIResources();
		this.sharedResources= SharedUIResources.getInstance();
		this.workbenchImages= PlatformUI.getWorkbench().getSharedImages();
	}
	
	
	@Override
	public @Nullable Image getImage(final @Nullable Object element) {
		if (element instanceof BuildpathListElement) {
			final BuildpathListElement listElement= (BuildpathListElement)element;
			ImageDescriptor imageDescriptor= getListElementBaseImage(listElement);
			if (imageDescriptor != null) {
				if (listElement.isMissing() || listElement.hasMissingChildren()) {
					imageDescriptor= new DecoratedElementImageDescriptor(imageDescriptor,
							DecoratedElementImageDescriptor.ERROR );
				}
				return this.sharedResources.getImageDescriptorRegistry().get(imageDescriptor);
			}
		}
		else if (element instanceof BuildpathListElementAttribute) {
			final BuildpathListElementAttribute attribute= (BuildpathListElementAttribute)element;
			switch (attribute.getName()) {
			case BuildpathAttribute.SOURCE_ATTACHMENT:
				return this.buildpathsResources.getImage(
						BuildpathsUIResources.OBJ_SOURCE_ATTACHMENT_ATTRIBUTE_IMAGE_ID );
			case BuildpathAttribute.FILTER_INCLUSIONS:
				return this.buildpathsResources.getImage(
						BuildpathsUIResources.OBJ_INCLUSION_FILTER_ATTRIBUTE_IMAGE_ID );
			case BuildpathAttribute.FILTER_EXCLUSIONS:
				return this.buildpathsResources.getImage(
						BuildpathsUIResources.OBJ_EXCLUSION_FILTER_ATTRIBUTE_IMAGE_ID );
			case BuildpathAttribute.OUTPUT:
				return this.buildpathsResources.getImage(
						BuildpathsUIResources.OBJ_OUTPUT_FOLDER_ATTRIBUTE_IMAGE_ID );
			default:
				break;
			}
		}
		return null;
	}
	
	@Override
	public String getText(final @Nullable Object element) {
		if (element instanceof BuildpathListElement) {
			return getListElementText((BuildpathListElement)element);
		}
		if (element instanceof BuildpathListElementAttribute) {
			final BuildpathListElementAttribute attribute= (BuildpathListElementAttribute)element;
			final String text= getListElementAttributeText(attribute);
			if (attribute.getStatus().getCode() == BuildpathInitializer.READ_ONLY) {
				return NLS.bind(Messages.ListLabel_Attribute_NonModifiable_label, text);
			}
			return text;
		}
		return super.getText(element);
	}
	
	
	private @Nullable ImageDescriptor getListElementBaseImage(final BuildpathListElement listElement) {
		switch (listElement.getType().getName()) {
		case BuildpathElement.PROJECT:
			return this.workbenchImages.getImageDescriptor(
					IDE.SharedImages.IMG_OBJ_PROJECT );
		case BuildpathElement.SOURCE:
			if (listElement.getPath().segmentCount() == 1) {
				return this.workbenchImages.getImageDescriptor(
						IDE.SharedImages.IMG_OBJ_PROJECT );
			}
			else {
				return this.workbenchImages.getImageDescriptor(
						ISharedImages.IMG_OBJ_FOLDER );
			}
		default:
			return null;
		}
	}
	
	public String getListElementText(final BuildpathListElement element) {
		final IPath path= element.getPath();
		switch (element.getType().getName()) {
		case BuildpathElement.PROJECT:
			String label= nonNullAssert(path.lastSegment());
			if (element.isMissing()) {
				label+= ' ' + Messages.ListLabel_Deco_Missing_label;
			}
			return label;
		case BuildpathElement.SOURCE: {
			final String pathLabel= getPathString(path, false);
			final StringBuilder sb= new StringBuilder(pathLabel);
			final IPath linkTarget= element.getLinkTarget();
			if (linkTarget != null) {
				sb.append(" - ");
				sb.append(MessageUtils.processPath(linkTarget.toOSString()));
			}
			final IResource resource= element.getResource();
			if (resource != null && !resource.exists()) {
				if (element.isMissing()) {
					sb.append(' ');
					sb.append(Messages.ListLabel_Deco_Missing_label);
				}
				else {
					sb.append(' ');
					sb.append(Messages.ListLabel_Deco_New_label);
				}
			}
			else if (element.getOrginalPath() == null) {
				sb.append(' ');
				sb.append(Messages.ListLabel_Deco_New_label);
			}
			return sb.toString();
		}
		default:
			return Messages.ListLabel_Element_Unknown_label;
		}
	}
	
	public String getListElementAttributeText(final BuildpathListElementAttribute attribute) {
		final String key= attribute.getName();
		switch (key) {
		case BuildpathAttribute.SOURCE_ATTACHMENT: {
				final String detail;
				final IPath path= (IPath)attribute.getValue();
				if (path != null && !path.isEmpty()) {
					if (attribute.getParent().getType().getName() == BuildpathElement.VARIABLE) {
						detail= getVariableString(path);
					}
					else {
						detail= getPathString(path, path.getDevice() != null);
					}
				}
				else {
					detail= Messages.ListLabel_Value_None_label;
				}
				return NLS.bind(Messages.ListLabel_Attribute_SourceAttachment_label, detail);
			}
		case BuildpathAttribute.FILTER_INCLUSIONS: {
				final String detail;
				final var patterns= (List<? extends IPath>)attribute.getValue();
				if (patterns != null && !patterns.isEmpty()) {
					final StringBuilder sb= new StringBuilder();
					final int patternsCount= appendPatternList(patterns, sb);
					if (patternsCount > 0) {
						detail= sb.toString();
					}
					else {
						detail= Messages.ListLabel_Value_Filter_None_label;
					}
				}
				else {
					detail= Messages.ListLabel_Value_Filter_All_label;
				}
				return NLS.bind(Messages.ListLabel_Attribute_Inclusion_label, detail);
			}
		case BuildpathAttribute.FILTER_EXCLUSIONS: {
				final String detail;
				final var patterns= (List<? extends IPath>)attribute.getValue();
				if (patterns != null && !patterns.isEmpty()) {
					final StringBuilder sb= new StringBuilder();
					final int patternsCount= appendPatternList(patterns, sb);
					if (patternsCount > 0) {
						detail= sb.toString();
					}
					else {
						detail= Messages.ListLabel_Value_Filter_None_label;
					}
				}
				else {
					detail= Messages.ListLabel_Value_Filter_None_label;
				}
				return NLS.bind(Messages.ListLabel_Attribute_Exclusion_label, detail);
			}
		case BuildpathAttribute.OUTPUT: {
				final String detail;
				final IPath path= (IPath)attribute.getValue();
				if (path != null) {
					detail= MessageUtils.processPath(path.toString());
				}
				else {
					detail= Messages.ListLabel_Value_Output_Default_label;
				}
				return NLS.bind(Messages.ListLabel_Attribute_OutputFolder_label, detail);
			}
		default: {
//				final ClasspathAttributeConfiguration config= this.fAttributeDescriptors.get(key);
//				if (config != null) {
//					final ClasspathAttributeAccess access= attribute.getClasspathAttributeAccess();
//					final String nameLabel= config.getNameLabel(access);
//					final String valueLabel= config.getValueLabel(access); // should be LTR marked
//					return Messages.format(Messages.ListLabel_Provider_attribute_label, new String[] { nameLabel, valueLabel });
//				}
				String value= (String)attribute.getValue();
				if (value == null) {
					value= Messages.ListLabel_Value_None_label;
				}
				return NLS.bind(Messages.ListLabel_Attribute_Generic_label, key, value);
			}
		}
	}
	
	private int appendPatternList(final List<? extends IPath> patterns, final StringBuilder sb) {
		int patternsCount= 0;
		for (final IPath pattern : patterns) {
			if (pattern.segmentCount() > 0) {
				final String text= MessageUtils.processPath(pattern.toString());
				if (patternsCount > 0) {
					sb.append(Messages.ListLabel_Value_Path_separator);
				}
				sb.append(text);
				patternsCount++;
			}
		}
		return patternsCount;
	}
	
	private String getPathString(final IPath path, final boolean isExternal) {
		return MessageUtils.processPath((isExternal) ? path.toOSString() : path.makeRelative().toString());
	}
	
	private String getVariableString(final IPath path) {
		return MessageUtils.processPath(path.toString());
	}
	
}
