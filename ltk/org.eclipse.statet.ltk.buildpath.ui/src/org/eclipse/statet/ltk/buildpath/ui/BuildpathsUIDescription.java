/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.buildpath.ui;

import java.util.List;

import org.eclipse.core.resources.IProject;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;

import org.eclipse.statet.ltk.buildpath.core.BuildpathElement;
import org.eclipse.statet.ltk.buildpath.core.BuildpathElementType;


public class BuildpathsUIDescription {
	
	
	public void toListElements(final IProject project, final List<BuildpathElement> coreElements,
			final List<BuildpathListElement> listElements) {
		for (final BuildpathElement element : coreElements) {
			listElements.add(new BuildpathListElement(project, null, element, false));
		}
	}
	
	public ImList<BuildpathElement> toCoreElements(final List<BuildpathListElement> listElements) {
		final BuildpathElement[] coreElements= new BuildpathElement[listElements.size()];
		int j= 0;
		for (final BuildpathListElement listElement : listElements) {
			coreElements[j++]= listElement.getCoreElement();
		}
		return ImCollections.newList(coreElements);
	}
	
	
	public BuildpathListElementComparator createListElementComparator() {
		return new BuildpathListElementComparator();
	}
	
	public BuildpathListLabelProvider createListLabelProvider() {
		return new BuildpathListLabelProvider();
	}
	
	
	public String getDefaultExt(final BuildpathListElement element) {
		return "ext";
	}
	
	
	public boolean getAllowAdd(final IProject project, final BuildpathElementType type) {
		return true;
	}
	
	public boolean getAllowEdit(final BuildpathListElement element) {
		return true;
	}
	
	public boolean getAllowEdit(final BuildpathListElementAttribute attribute) {
		return true;
	}
	
}
