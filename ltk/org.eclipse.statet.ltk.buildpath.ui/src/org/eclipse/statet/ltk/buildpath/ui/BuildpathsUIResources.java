/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.buildpath.ui;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.ecommons.ui.util.UIResources;

import org.eclipse.statet.internal.ltk.buildpath.ui.BuildpathsUIPlugin;


@NonNullByDefault
public class BuildpathsUIResources extends UIResources {
	
	
	private static final String NS= "org.eclipse.statet.ltk.buildpath"; //$NON-NLS-1$
	
	
	public static final String OBJ_SOURCE_ATTACHMENT_ATTRIBUTE_IMAGE_ID= NS + "/image/obj/attribute-source_attachment"; //$NON-NLS-1$
	public static final String OBJ_INCLUSION_FILTER_ATTRIBUTE_IMAGE_ID= NS + "/image/obj/attribute-inclusion_filter"; //$NON-NLS-1$
	public static final String OBJ_EXCLUSION_FILTER_ATTRIBUTE_IMAGE_ID= NS + "/image/obj/attribute-exclusion_filter"; //$NON-NLS-1$
	public static final String OBJ_OUTPUT_FOLDER_ATTRIBUTE_IMAGE_ID= NS + "/image/obj/attribute-output_folder"; //$NON-NLS-1$
	
	
	static final BuildpathsUIResources INSTANCE= new BuildpathsUIResources();
	
	
	private BuildpathsUIResources() {
		super(BuildpathsUIPlugin.getInstance().getImageRegistry());
	}
	
	
}
