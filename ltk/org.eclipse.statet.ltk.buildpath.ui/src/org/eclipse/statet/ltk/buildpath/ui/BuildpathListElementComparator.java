/*=============================================================================#
 # Copyright (c) 2000, 2022 IBM Corporation and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0
 # 
 # Contributors:
 #     IBM Corporation - org.eclipse.jdt: initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.ltk.buildpath.ui;

import org.eclipse.jface.viewers.ContentViewer;
import org.eclipse.jface.viewers.IBaseLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerComparator;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.ltk.buildpath.core.BuildpathElement;


@NonNullByDefault
public class BuildpathListElementComparator extends ViewerComparator {
	
	
	protected static final int SOURCE= 0;
	protected static final int PROJECT= 1;
	protected static final int LIBRARY= 2;
	protected static final int VARIABLE= 3;
	protected static final int CONTAINER= 4;
	
	protected static final int ATTRIBUTE= 11;
	protected static final int CONTAINER_ENTRY= 12;
	
	protected static final int OTHER= 100;
	
	
	public BuildpathListElementComparator() {
	}
	
	
	@Override
	public int category(final Object obj) {
		if (obj instanceof BuildpathListElement) {
			final BuildpathListElement element= (BuildpathListElement) obj;
			if (element.getParent() != null) {
				return CONTAINER_ENTRY;
			}
			return getCategory(element);
		}
		else if (obj instanceof BuildpathListElementAttribute) {
			return ATTRIBUTE;
		}
		return OTHER;
	}
	
	protected int getCategory(final BuildpathListElement element) {
		final String typeName= element.getType().getName();
		if (typeName == BuildpathElement.SOURCE) {
			return SOURCE;
		}
		else if (typeName == BuildpathElement.PROJECT) {
			return PROJECT;
		}
		else if (typeName == BuildpathElement.LIBRARY) {
			return LIBRARY;
		}
		else if (typeName == BuildpathElement.VARIABLE) {
			return VARIABLE;
		}
		else {
			return OTHER;
		}
	}
	
	@Override
	public int compare(final @Nullable Viewer viewer, final Object e1, final Object e2) {
		final int cat1= category(e1);
		final int cat2= category(e2);
		
		if (cat1 != cat2) {
			return cat1 - cat2;
		}
		if (cat1 == ATTRIBUTE || cat1 == CONTAINER_ENTRY) {
			return 0; // do not sort attributes or container entries
		}
		if (viewer instanceof ContentViewer) {
			final IBaseLabelProvider prov= ((ContentViewer) viewer).getLabelProvider();
			if (prov instanceof ILabelProvider) {
				final ILabelProvider lprov= (ILabelProvider) prov;
				final String name1= lprov.getText(e1);
				final String name2= lprov.getText(e2);
				return getComparator().compare(name1, name2);
			}
		}
		return 0;
	}
	
}
