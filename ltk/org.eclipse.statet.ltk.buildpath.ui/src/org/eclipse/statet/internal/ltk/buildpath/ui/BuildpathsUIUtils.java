/*=============================================================================#
 # Copyright (c) 2016, 2022 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.eclipse.statet.internal.ltk.buildpath.ui;

import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;

import org.eclipse.statet.ltk.buildpath.core.BuildpathElement;
import org.eclipse.statet.ltk.buildpath.ui.BuildpathListElement;


public class BuildpathsUIUtils {
	
	
	public static boolean isProjectSourceFolder(final List<BuildpathListElement> listElements,
			final IProject project) {
		final IPath projectPath= project.getProject().getFullPath();
		for (final BuildpathListElement listElement : listElements) {
			final BuildpathElement element= listElement.getCoreElement();
			if (element.getTypeName() == BuildpathElement.SOURCE) {
				if (projectPath.equals(element.getPath())) {
					return true;
				}
			}
		}
		return false;
	}
	
	public static void insert(final List<BuildpathListElement> list, final BuildpathListElement element) {
		final int length= list.size();
		int i= 0;
		while (i < length && list.get(i).getType() != element.getType()) {
			i++;
		}
		if (i < length) {
			i++;
			while (i < length && list.get(i).getType() == element.getType()) {
				i++;
			}
			list.add(i, element);
			return;
		}
		
		switch (element.getType().getName()) {
		case BuildpathElement.SOURCE:
			list.add(0, element);
			break;
		case BuildpathElement.PROJECT:
		default:
			list.add(element);
			break;
		}
	}
	
	
	public static BuildpathElement[] convertToCoreElements(final List<BuildpathListElement> listElements) {
		final BuildpathElement[] result= new BuildpathElement[listElements.size()];
		int i= 0;
		for (final Iterator<BuildpathListElement> iter= listElements.iterator(); iter.hasNext();) {
			final BuildpathListElement cur= iter.next();
			result[i]= cur.getCoreElement();
			i++;
		}
		return result;
	}
	
}
